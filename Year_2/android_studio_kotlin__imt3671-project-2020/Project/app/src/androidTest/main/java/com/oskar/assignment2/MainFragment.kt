package com.oskar.assignment2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.oskar.assignment2.data.MainViewModel
import kotlinx.android.synthetic.main.main_fragment.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe

class MainFragment : Fragment() {

    var main = MainActivity()

    private val viewModel : MainViewModel by viewModels()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.main_fragment,container,false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //Set up recycler view
        recycler_view.apply {
            adapter = Adapter()
            layoutManager = LinearLayoutManager(requireContext())


        }

        //Gets list of recycler view items
        viewModel.trips.observe(viewLifecycleOwner) {
            (recycler_view.adapter as Adapter).submitList(it)
        }


    }

    //Gets mainactivity object
    fun getMainActivity(act : MainActivity) {

        main = act
    }


}