package com.oskar.assignment2

import android.content.ContentValues
import android.content.Intent
import android.content.SharedPreferences
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.oskar.assignment2.databinding.ActivityMainBinding
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_sensor.view.*


class MainActivity : AppCompatActivity(), SensorEventListener {

    private var token: String? = null
    var id = 1

    //step counter
    private var steps = 0
    private var running = false
    private var sensorManager: SensorManager? = null

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}
    // get the number of step and display for user
    override fun onSensorChanged(event: SensorEvent?) {
        if (sensorManager != null && running){
            steps = event!!.values[0].toInt()
            //Toast.makeText(this,  event.values[0].toInt().toString(), Toast.LENGTH_LONG).show()
            val txt = "Steps: $steps"
            txt_steps.text = txt
            txt_steps.visibility = View.VISIBLE

        }
    }
    // OnResume: ready to run
    override fun onResume() {
        super.onResume()
        // continue counting
        //running = true
        running = true
        val stepsSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
        // check if stepSensor
        if(stepsSensor == null){
            Toast.makeText(this, "No Step Counter Sensor !", Toast.LENGTH_SHORT).show()
        }
        // else registerListener
        else{
            sensorManager?.registerListener(this, stepsSensor, SensorManager.SENSOR_DELAY_UI)
        }
    }
    // stop counting steps
    override fun onPause() {
        super.onPause()
        //running = false
        running = false
        // stop counting steps
        sensorManager?.unregisterListener(this)
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //---------------------
        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        //----------------------

        /* Set the action bar that we are using */
        setSupportActionBar(binding.toolbar)

        /* Configure our app bar to have 1 home destination and a drawer */
        val navController = findNavController(R.id.nav_fragment)
        val appBarConfig = AppBarConfiguration(navController.graph, binding.mainView)

        /* Hook up toolbar with nav controller */
        binding.toolbar.setupWithNavController(navController, appBarConfig)

        /* Hook up drawer with nav controller */
        binding.navigationView.setupWithNavController(navController)

        /* Hook up bottom nav bar with nav controller */
        binding.bottomNav.setupWithNavController(navController)


        // Gets dark mode preference
        val appSettingPrefs : SharedPreferences = getSharedPreferences("AppSettingPrefs", 0)
        val isNightModeOn: Boolean = appSettingPrefs.getBoolean("NightMode", false)

        //Sets night mode on or off
        if(isNightModeOn) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
        else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }

        img_button_map.setOnClickListener {
            val intent = Intent(this, Settings::class.java)
            startActivity(intent)
        }

        //Set up firebase
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if(!task.isSuccessful) {
                    Log.w(ContentValues.TAG, "GetInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                val temp = task.result?.token
                token = temp
            })
    }
    override fun onSupportNavigateUp() = findNavController(R.id.nav_fragment).navigateUp()


}



