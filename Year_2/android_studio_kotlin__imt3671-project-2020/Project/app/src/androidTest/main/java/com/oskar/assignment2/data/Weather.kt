package com.oskar.assignment2.data

data class Weather (
    val main : Main
)

data class Main (
    val temp : Double,
    val feels_like : Double,
    val humidity : Int,
    val temp_min : Double,
    val temp_max : Double
)
