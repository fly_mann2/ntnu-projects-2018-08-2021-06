package com.oskar.assignment2

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.hardware.SensorManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.firebase.firestore.SetOptions
import com.google.gson.Gson
import com.oskar.assignment2.data.*
import kotlinx.android.synthetic.main.fragment_map.*
import kotlinx.android.synthetic.main.fragment_map.edit_location
import kotlinx.android.synthetic.main.fragment_map.view.*
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

// implement the GoogleMap.OnMarkerClickListener interface, which defines the onMarkerClick()
@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
// , OnMapReadyCallback, GoogleMap.OnMarkerClickListener
class FragmentMap : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener  {

    private var mainAct = MainActivity()

    private var tripsDao: TripsDao? = null
    val database = AppDatabase.getDatabase(mainAct)


    lateinit var lastLocation: Location
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private  var locationUpdateState = false

    private var location1 = LatLng(59.423839,5.282924)
    private var location2 = LatLng(59.423839,5.272924)
    private var location3 = LatLng(59.425513,5.275690)

    private var countM = 0


    var startLoc = ""
    var endLoc = ""
    var dist =""
    var duration = ""




    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val REQUEST_CHECK_SETTINGS = 2
    }

    private fun getDirectionURL(origin: LatLng, dest:LatLng) : String{
        // walking
        return "https://maps.googleapis.com/maps/api/directions/json?origin=${origin.latitude},${origin.longitude} " +
                "&destination=${dest.latitude},${dest.longitude}&sensor=false&mode=walking&key=AIzaSyBiS5CYkCoyfJhq7GORvOhF6yHkUo1bruc"
    }
    // get MainActivity
    fun getMainActivity(act : MainActivity) {
        mainAct = act
    }
    override fun onMarkerClick(p0: Marker?) = false


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        map_view.onCreate(savedInstanceState)
        map_view.onResume()
        // automatically initializes the maps system and the view
        map_view.getMapAsync(this)
        Log.v("yyyy", "ssss")

        // from OnCreate
        val database = AppDatabase.getDatabase(mainAct)
        tripsDao = database.tripsDao()
        // FusedLocationProviderClient: GPS location and network location for balance between battery consumption and accuracy
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(mainAct)
        locationCallback = object  : LocationCallback(){
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                // update lastLocation
                lastLocation = p0!!.lastLocation
                // update the map with the new location coordinates
                placeMarkerOnMap(LatLng(lastLocation.latitude, lastLocation.longitude))
            }
        }
        createLocationRequest()
    }

    // Override onPause() to stop location update request
    override fun onPause(){
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
        map_view.onPause()
    }
    // Override onResume() to restart the location update request
    override fun onResume(){
        super.onResume()
        map_view.onResume()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())
        if (!locationUpdateState){ startLocationUpdates() }
    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        val view : View =  inflater.inflate(R.layout.fragment_map, container, false)
        view.btn_route.setOnClickListener {
            map.isMyLocationEnabled = true // maybe remove
            // Remove marker
            map.clear()
            map.setOnMapClickListener {
                //map.addMarker(it)
                when (countM) {
                    0 -> {
                        countM++
                        location2 = it
                        map.addMarker(location2)
                        map.addMarker(MarkerOptions().position(location2))
                    }
                    1 -> {
                        countM++
                        location3 = it
                        displayRoute(location2,location3)
                    }
                    else -> {
                        countM = 0
                        map.clear()
                    }
                }
            }
        }
        // Go To/search button. search after location
        view.btn_picker.setOnClickListener {
            searchLocation(edit_location.text.toString())
        }
        view.btn_save.setOnClickListener {
            // Save Route
            val data = Trips(null,startLoc,endLoc,dist,duration)
            tripsDao!!.insert(data)

            addToDb(startLoc,endLoc,dist,duration)

            // mainAct.id ++


        }
        return view
    }

    private fun addToDb(startLoc: String?, endLoc: String?, dist: String?, duration: String?) {
        try {
            if(startLoc == "" || endLoc == "") {
                throw Exception("Please choose start and end points")

            }else {
                val db = FireDB.getDB()

                if(db != null) {
                    val data = Trip(startLoc, endLoc, dist,duration)
                    db.collection("trips").document()
                        .set(data, SetOptions.merge())
                        .addOnSuccessListener {
                            // mainAct.showMainFragment()
                            Log.w("Firebase", "Success")
                        }.addOnFailureListener {
                            throw it
                        }

                }
            }

        } catch (Err: Exception) {
            Log.d("Exception addToDb ", "${Err.message}")

        }
    }

    // check if the app has been granted the ACCESS_FINE_LOCATION
    // If it hasn't, then request it from the user
    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission( mainAct, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions( mainAct, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
        // enables the my-location layer which draws a light blue dot on the user’s location
        map.isMyLocationEnabled = true
        // change Terrain
        map.mapType = GoogleMap.MAP_TYPE_TERRAIN
        // gives you the most recent location currently available
        fusedLocationClient.lastLocation.addOnSuccessListener(mainAct) { location ->
            // camera to the user’s current location
            if(location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                placeMarkerOnMap(currentLatLng)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }

    // In startLocationUpdates(), if the ACCESS_FINE_LOCATION permission has not been granted, request it now and return.
    private fun startLocationUpdates(){
        if (ActivityCompat.checkSelfPermission(mainAct, android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(mainAct, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
        // If there is permission, request for location updates.
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null /* Looper*/)
    }
    private fun createLocationRequest(){
        // create an instance of LocationRequest, add to LocationSettingsRequest.Builder and retrieve and handle
        locationRequest = LocationRequest()
        // interval specifies the rate at which your app will like to receive updates
        locationRequest.interval = 10000
        // fastestInterval specifies the fastest rate at which the app can handle updates.
        locationRequest.fastestInterval = 5000
        // create a settings client and a task to check location settings
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        val client = LocationServices.getSettingsClient(mainAct)
        val task = client.checkLocationSettings(builder.build())
        //  initiate a location request
        task.addOnSuccessListener{
            locationUpdateState = true
            startLocationUpdates()
        }
        // failure: settings have some issues which can be fixed
        task.addOnFailureListener{
            if (it is ResolvableApiException){
                try {
                    it.startResolutionForResult(mainAct, REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException){

                }
            }
        }
    }
    inner class GetDirection(val url : String) : AsyncTask<Void,Void,List<List<LatLng>>>(){
        override fun doInBackground(vararg params: Void?) : List<List<LatLng>>{
            val client = OkHttpClient()
            val request  = Request.Builder().url(url).build()
            val response = client.newCall(request).execute()
            val data = response.body()!!.string()
            Log.d("map" , " data : $data")
            val result =ArrayList<List<LatLng>>()
            try {
                // Google map information Gson
                val respObj = Gson().fromJson (data, GoogleMapDTO::class.java)
                //val e = DirectionsJSONParser(respObj)
                val path = ArrayList<LatLng>()

                for (i in 0..(respObj.routes[0].legs[0].steps.size-1)){
                    path.addAll(decodePolyline(respObj.routes[0].legs[0].steps[i].polyline.points))
                    respObj.routes[0].legs[0].distance
                    startLoc = respObj.routes[0].legs[0].start_address
                    endLoc = respObj.routes[0].legs[0].end_address
                    dist = respObj.routes[0].legs[0].distance.text
                    duration = respObj.routes[0].legs[0].duration.text
                    //Log.d("xxx", decodePolyline(respObj.routes[0].legs[0].steps[i].polyline.points).toString())

                }
                result.add(path)
            }catch (e:Exception){
                e.printStackTrace()
            }
            return result
        }

        override fun onPostExecute(result: List<List<LatLng>>?) {
            val lineoption = PolylineOptions()
            for (i in result!!.indices){
                lineoption.addAll(result[i])
                lineoption.width(10f)
                lineoption.color(Color.BLUE)
                lineoption.geodesic(true)
            }
            map.addPolyline(lineoption)
        }
    }

    private fun GoogleMap.addMarker(it: LatLng?) { }

    private fun displayRoute(origin: LatLng, dest:LatLng) {
        map.addMarker(dest)
        map.addMarker(MarkerOptions().position(dest))
        val URL = getDirectionURL(origin, dest)
        GetDirection(URL).execute()
    }

    fun decodePolyline(encoded: String): List<LatLng> {

        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val latLng = LatLng((lat.toDouble() / 1E5),(lng.toDouble() / 1E5))
            poly.add(latLng)
        }

        return poly
    }

    private fun placeMarkerOnMap(location: LatLng){
        // Create a MarkerOptions
        val markerOptions = MarkerOptions().position(location)
        // change marker icon
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(
            BitmapFactory.decodeResource(resources, R.mipmap.ic_user_location)))

        val titleStr = getAddress(location)
        markerOptions.title(titleStr)

        // Add the marker to the map
        map.addMarker(markerOptions)
    }

    override fun onMapReady(map: GoogleMap?) {
        // check i f can get map
        map?.let {
            this.map = it
            // enable zoom func
            map?.uiSettings!!.isZoomControlsEnabled = true
            map.setOnMarkerClickListener(this)
            setUpMap()
        }

    }

    private fun searchLocation(location: String){
        var addressList : List<Address>? = null
        val options = MarkerOptions()
        // if location is not empty
        if(location != ""){
            val geocode = Geocoder(mainAct)
            try {
                // Returns an array of Addresses that are known to describe the named location
                addressList = geocode.getFromLocationName(location, 5)
            }catch (e: IOException){
                e.printStackTrace()
            }
            // goes try addressList
            for (i in addressList!!.indices){
                val address = addressList[i]
                val latLng = LatLng(address.latitude,  address.longitude)
                options.position(latLng)
                map.addMarker(options)
                // move camera "animated" to location
                map.animateCamera(CameraUpdateFactory.newLatLng(latLng))
            }
        }
    }

    private fun getAddress(latLng: LatLng): String {
        // turn a latitude and longitude coordinate into an address and vice versa
        val geocode = Geocoder(this.context)
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""


        try {
            // Asks the geocode to get the address from the location
            addresses = geocode.getFromLocation(latLng.latitude, latLng.longitude, 1)
            // contains any address, then append it to a string and return
            if ( null != addresses && addresses.isNotEmpty()){
                address = addresses[0]
                (0 .. address.maxAddressLineIndex).forEach { i ->
                    val addressLine = address.getAddressLine(i)
                    addressText +=
                        if (i == 0) {
                            addressLine
                        } else "\n" + addressLine
                }
            }
        } catch (e: IOException){
            Log.v("FragmentMap", e.localizedMessage)
        }
        return addressText
    }

}


