package com.oskar.assignment2

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_sensor.view.textView_step_counter_view


class FragmentSensor : Fragment(), SensorEventListener  {

    private var steps = 0
    private var running = false
    private var sensorManager:SensorManager? = null



    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}
    // get the number of step and display for user
    override fun onSensorChanged(event: SensorEvent?) {
        if(running){
            steps = event!!.values[0].toInt()
            view?.textView_step_counter_view?.text = steps.toString()
        }
    }
    // OnResume: ready to run
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onResume() {
        super.onResume()
        // continue counting
        //running = true
        running = true
        val stepsSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
        // check if stepSensor
        if(stepsSensor == null){
            Toast.makeText(activity, "No Step Counter Sensor !", Toast.LENGTH_SHORT).show()
        }
        // else registerListener
        else{
            sensorManager?.registerListener(this, stepsSensor, SensorManager.SENSOR_DELAY_UI)
        }
    }
    // stop counting steps
    override fun onPause() {
        super.onPause()
        //running = false
        running = false
        // stop counting steps
        sensorManager?.unregisterListener(this)
    }
    // right now just for onSensorChanged/UI
    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //val view: View = inflater.inflate(R.layout.fragment_sensor, container, false)
        //viewBinding = FragmentSensorBinding.inflate(inflater, container, false)


        //return viewBinding.root
        return inflater.inflate(R.layout.fragment_sensor, container, false)
        //return view

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sensorManager = activity?.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }
}
