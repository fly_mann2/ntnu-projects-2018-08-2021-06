package com.oskar.assignment2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oskar.assignment2.data.TripsData
import kotlinx.android.synthetic.main.trip_item.view.*





class FirebaseAdapter(private val tripsData: List<TripsData>) : RecyclerView.Adapter<FirebaseAdapter.ViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.trip_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount() = tripsData.size

    //Bind data to text views in recycler view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val td: TripsData = tripsData[position]
        holder.locstart.text = td.getStart()
        holder.locend.text = td.getEnd()
        holder.dist.text = td.getDistance()
        holder.duration.text = td.getDuration()
        //holder.loc.setText(td.getLocation())
        //holder.loc.text = td.getLocation()
        //holder.loc.setText(td.getLocation())

    }

    //Gets the text views from trip_tiem
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val locstart: TextView = itemView.start
            val locend: TextView = itemView.end
            val dist: TextView = itemView.dist
            val duration: TextView = itemView.duration
            //val loc2: TextView = itemView.textView2

    }


}