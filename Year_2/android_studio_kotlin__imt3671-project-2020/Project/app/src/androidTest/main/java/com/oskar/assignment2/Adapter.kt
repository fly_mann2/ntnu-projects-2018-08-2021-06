package com.oskar.assignment2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ListAdapter
import com.oskar.assignment2.data.Trips


class Adapter : ListAdapter<Trips, Adapter.ViewHolder> (EntryDifferenceCallback()) {

    class ViewHolder(val binding : View) : RecyclerView.ViewHolder(binding)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.trip_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val fname = holder.binding.findViewById<TextView>(R.id.start)
        val lname = holder.binding.findViewById<TextView>(R.id.end)
        val dist = holder.binding.findViewById<TextView>(R.id.dist)
        val time = holder.binding.findViewById<TextView>(R.id.duration)

        fname.text = getItem(position).startLoc.toString()
        lname.text = getItem(position).endLoc.toString()
        dist.text = getItem(position).distance.toString()
        time.text = getItem(position).duration.toString()
    }


    class EntryDifferenceCallback : DiffUtil.ItemCallback<Trips>() {
        override fun areItemsTheSame(oldItem: Trips, newItem: Trips): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Trips, newItem: Trips): Boolean {
            return oldItem.startLoc == newItem.startLoc && oldItem.endLoc == newItem.endLoc
        }
    }

}