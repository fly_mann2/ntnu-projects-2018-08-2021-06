package com.oskar.assignment2.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Trips::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    /**
     * This is the User data access object instance
     * @return the dao to user database operations
     */
    abstract fun tripsDao(): TripsDao

    companion object {

        /**
         * This is just for singleton pattern
         */
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context?): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class.java) {
                    if (INSTANCE == null) {
                        // Get PhraseRoomDatabase database instance
                        INSTANCE = Room.databaseBuilder(context!!.applicationContext,
                            AppDatabase::class.java, "trips_database"
                        )
                            .allowMainThreadQueries()
                            .build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}