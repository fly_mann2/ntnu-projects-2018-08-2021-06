package com.oskar.assignment2.data

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkViewModel : ViewModel() {

    var city = ""

    private val retrofit = Retrofit.Builder()
        .baseUrl("http://api.openweathermap.org/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun getWeather() = liveData {
        val service = retrofit.create(WeatherEndpoint::class.java)
        val call = service.getWeather(city)
        val response = withContext(Dispatchers.IO) { call.execute() }
        emit(response.body())
    }
}
