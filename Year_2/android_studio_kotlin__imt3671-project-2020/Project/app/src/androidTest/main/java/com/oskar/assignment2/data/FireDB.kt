package com.oskar.assignment2.data

import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class FireDB {

    companion object {
        fun getDB() : FirebaseFirestore? { //Get db instance
            return try {
                val db: FirebaseFirestore = Firebase.firestore
                db
            } catch (Err: FirebaseFirestoreException) {
                Log.d("Firestore", Err.code.toString())
                null
            }
        }



    }
}