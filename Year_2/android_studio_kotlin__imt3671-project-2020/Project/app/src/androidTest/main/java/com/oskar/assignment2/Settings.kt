package com.oskar.assignment2

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import com.oskar.assignment2.data.AppDatabase
import com.oskar.assignment2.data.TripsDao
import kotlinx.android.synthetic.main.settings.*

class Settings : AppCompatActivity() {

    private var tripsDao: TripsDao? = null
    val database = AppDatabase.getDatabase(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings)

        val database = AppDatabase.getDatabase(this)
        tripsDao = database.tripsDao()

        //Sets up and save dark mode preference
        val appSettingPrefs : SharedPreferences = getSharedPreferences("AppSettingPrefs", 0)
        val editPrefs : SharedPreferences.Editor = appSettingPrefs.edit()
        val isNightModeOn: Boolean = appSettingPrefs.getBoolean("NightMode", false)

        //Sets dark mode on or off, check box checked/unchecked
        if(isNightModeOn) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            check_dark.isChecked = true
        }
        else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }

        check_dark.setOnClickListener {
            //Turn dark mode on or off with the check box
            if (isNightModeOn) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                editPrefs.putBoolean("NightMode", false)
                editPrefs.apply()
                check_dark.isChecked = false
            }
            else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                editPrefs.putBoolean("NightMode", true)
                editPrefs.apply()
                check_dark.isChecked = true
            }

        }

        btn_delete.setOnClickListener{
            btn_delete.visibility = View.INVISIBLE
            txt_delete.visibility = View.INVISIBLE
            txt_confirm.visibility = View.VISIBLE
            btn_yes.visibility = View.VISIBLE
            btn_no.visibility = View.VISIBLE
        }

        btn_no.setOnClickListener{
            btn_delete.visibility = View.VISIBLE
            txt_delete.visibility = View.VISIBLE
            txt_confirm.visibility = View.INVISIBLE
            btn_yes.visibility = View.INVISIBLE
            btn_no.visibility = View.INVISIBLE
        }

        btn_yes.setOnClickListener {
            btn_delete.visibility = View.VISIBLE
            txt_delete.visibility = View.VISIBLE
            txt_confirm.visibility = View.INVISIBLE
            btn_yes.visibility = View.INVISIBLE
            btn_no.visibility = View.INVISIBLE
            tripsDao!!.deleteAll()
            Toast.makeText(this, "Local data deleted!", Toast.LENGTH_SHORT).show()
        }


    }
}
