package com.oskar.assignment2

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.Source
import com.google.firebase.firestore.ktx.toObject
import com.oskar.assignment2.data.FireDB
import com.oskar.assignment2.data.TripsData
import com.oskar.assignment2.data.tripsData
import kotlinx.android.synthetic.main.trips_fragment.*


class TripsFragment: Fragment(){

    //private var tripsData: List<TripsData>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.trips_fragment, container, false)



    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        //Gets all data from firebase
        try {
            val db = FireDB.getDB()
            if (db != null) {
                val query = db.collection("trips")
                query.get(Source.SERVER)
                    .addOnSuccessListener { documents ->
                        tripsData.clear()       //First clears the array of data to avoid duplicates
                        for (document in documents) {
                            if(document != null) {
                                val t  = document.toObject<TripsData>()

                                tripsData.add(t)       //Adds the data to the array
                            }
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.w(ContentValues.TAG, "Error getting data from database", exception)

                    }
            }
        } catch (Err: Exception) {
            Log.d("Exception getting trip data ","${Err.message}")
        }

        //Set up recycler view
        recyclerview_trips.apply {
            adapter = FirebaseAdapter(tripsData)
            layoutManager = LinearLayoutManager(requireContext())
        }


    }



}