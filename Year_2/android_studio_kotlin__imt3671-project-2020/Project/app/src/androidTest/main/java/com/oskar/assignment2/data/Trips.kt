package com.oskar.assignment2.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "trip_table")
data class Trips (
    @PrimaryKey(autoGenerate = true) var id: Int? = null,
    @ColumnInfo(name = "location_start") val startLoc: String?,
    @ColumnInfo(name = "location_end") val endLoc: String?,
    @ColumnInfo(name = "distance") val distance: String?,
    @ColumnInfo(name = "duration") val duration: String?


)