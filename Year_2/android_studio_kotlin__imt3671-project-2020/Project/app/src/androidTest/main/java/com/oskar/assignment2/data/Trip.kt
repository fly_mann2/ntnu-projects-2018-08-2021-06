package com.oskar.assignment2.data

data class Trip (
    val start: String? = null,
    val end: String? = null,
    val distance: String? = null,
    val duration: String? = null

)