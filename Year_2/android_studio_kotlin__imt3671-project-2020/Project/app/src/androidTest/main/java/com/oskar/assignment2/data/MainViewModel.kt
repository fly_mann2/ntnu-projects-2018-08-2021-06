package com.oskar.assignment2.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.liveData

class MainViewModel(application: Application) : AndroidViewModel(application) {

    //private val database = AppDatabase.getDatabase(application)
    //private val phraseDao = database.userDao()


    /** Live data access to the database of repositories */
    val trips = liveData {
        emitSource(AppDatabase.getDatabase(application).tripsDao().getAll())
    }
}