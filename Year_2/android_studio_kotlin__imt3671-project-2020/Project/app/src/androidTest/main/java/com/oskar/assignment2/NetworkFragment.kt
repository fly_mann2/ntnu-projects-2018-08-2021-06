package com.oskar.assignment2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.oskar.assignment2.data.NetworkViewModel
import com.oskar.assignment2.databinding.FragmentNetworkBinding
import kotlinx.android.synthetic.main.fragment_network.*


class NetworkFragment : Fragment() {
    private val viewModel: NetworkViewModel by activityViewModels()

    private lateinit var viewBinding: FragmentNetworkBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = FragmentNetworkBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_citySearch.setOnClickListener {
            viewModel.city = (weatherCity.text?.toString() ?: "")
            updateWeather()
        }

    }

    private fun updateWeather() {
        viewModel.getWeather().observe(viewLifecycleOwner, Observer {
            it?.let { weather ->
                viewBinding.weatherTemp.text = kelvinToCelsius(weather.main.temp.toString())
                viewBinding.weatherFeelsLike.text = kelvinToCelsius(weather.main.feels_like.toString())
                viewBinding.weatherHumidity.text = weather.main.humidity.toString()
                viewBinding.weatherMinTemp.text = kelvinToCelsius(weather.main.temp_min.toString())
                viewBinding.weatherMaxTemp.text = kelvinToCelsius(weather.main.temp_max.toString())
                viewBinding.weatherQuery.text = viewModel.city
            }
        })
    }

    private fun kelvinToCelsius(kelvin: String) : String {
        var temp = kelvin.toFloat()
        temp -= 273.15f
        return temp.toString()
    }
}
