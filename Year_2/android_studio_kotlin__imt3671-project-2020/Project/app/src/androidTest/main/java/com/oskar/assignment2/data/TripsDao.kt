package com.oskar.assignment2.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TripsDao {
    @Query("SELECT * FROM trip_table")
    fun getAll(): LiveData<List<Trips>>

    @Query("SELECT * FROM trip_table WHERE id IN (:tripIds)")
    fun loadAllByIds(tripIds: IntArray): LiveData<List<Trips>>

    //@Query("SELECT * FROM trip_table WHERE first_name LIKE :first AND " +
    //        "last_name LIKE :last LIMIT 1")
    //fun findByName(first: String, last: String): Trips

    @Insert
    fun insert(vararg item: Trips)
    //fun insertAll(vararg trips: Trips)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun  insertOne(data: List<Trips>)

    @Delete
    fun delete(trips: Trips)
    @Query("DELETE FROM trip_table")
    fun deleteAll()
}