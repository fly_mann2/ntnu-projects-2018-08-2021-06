package com.oskar.assignment2.data

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherEndpoint {
    @GET("data/2.5/weather?appid=8f8a02768685ea333e3858270ed9bb91")
        fun getWeather(@Query("q") q: String) : Call<Weather>
}