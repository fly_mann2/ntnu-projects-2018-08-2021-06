package com.oskar.assignment2

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import kotlinx.coroutines.delay
import org.hamcrest.core.StringContains.containsString
import org.hamcrest.CoreMatchers.allOf
import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class AcceptanceTests {

    @get:Rule
    val intentsRule = IntentsTestRule(MainActivity::class.java)

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.oskar.assignment2", appContext.packageName)
    }

    /**
     * Checks if pressing database button switches to database fragment
     */
    //@Test
    //fun clickBtnDb_viewChange() {
    //    onView(withId(R.id.btn_db))
    //        .perform(click())
//
    //    onView(withId(R.id.database_view))
    //        .check(matches(isDisplayed()))
    //}

    /**
     * Checks if pressing map button switches to map fragment
     */
   //@Test
   //fun clickBtnMap_viewChange() {
   //    onView(withId(R.id.btn_map))
   //        .perform(click())

   //    onView(withId(R.id.map_view))
   //        .check(matches(isDisplayed()))
   //}

    /**
     * Checks if pressing steps button switches to steps fragment
     */
    //@Test
    //fun clickBtnSteps_viewChange() {
    //    onView(withId(R.id.btn_step_counter))
    //        .perform(click())
//
    //    onView(withId(R.id.textView_step_counter_view))
    //        .check(matches(isDisplayed()))
    //}

    /**
     * Checks if pressing settings button switches to settings activity
     */
    //@Test
    //fun clickBtnSettings_viewChange() {
    //    onView(withId(R.id.btn_settings))
    //        .perform(click())
//
    //    onView(withId(R.id.check_dark))
    //        .check(matches(isDisplayed()))
    //}

    /**
     * Checks if pressing start button switches back to main fragment
     */
    //@Test
    //fun clickBtnStart_viewChange() {
    //    onView(withId(R.id.btn_step_counter))
    //        .perform(click())
//
    //    onView(withId(R.id.textView_step_counter_view))
    //        .check(matches(isDisplayed()))
//
    //    onView(withId(R.id.btn_back))
    //        .perform(click())
//
    //    onView(withId(R.id.recycler_view))
    //        .check(matches(isDisplayed()))
    //}
    /**
     * Checks if pressing network button switches to network fragment and a Chuck Norris joke is displayed
     */
    //@Test
    //fun clickBtnNetwork_viewChange() {
//
    //    onView(withId(R.id.btn_network))
    //        .perform(click())
//
    //    //Sleeps so that the joke has time to show up
    //    Thread.sleep(1500)
//
    //    onView(withId(R.id.jokeText))
    //        .check(matches(withText(containsString("Chuck"))))
//
    //}



    /**
     * Checks if item added to database is displayed in the recycler view
     */
    //@Test
    //fun addToDb_SeeInRecyclerView() {
    //    val firstName = "Bob"
    //    val lastName = "Bobsen"
//
    //    onView(withId(R.id.btn_db))
    //        .perform(click())
//
    //    onView(withId(R.id.database_view))
    //        .check(matches(isDisplayed()))
//
//
    //    onView(withId(R.id.edit_firstname))
    //        .perform(replaceText(firstName))
//
    //    onView(withId(R.id.edit_lastname))
    //        .perform(replaceText(lastName))
//
    //    onView(withId(R.id.btn_add))
    //        .perform(click())
//
    //    onView(withId(R.id.recycler_view))
    //        .check(matches(isDisplayed()))
//
    //    onView(allOf(withText(firstName), isDescendantOfA(withId(R.id.recycler_view))))
    //        .check(matches(isDisplayed()))
//
    //    onView(allOf(withText(lastName), isDescendantOfA(withId(R.id.recycler_view))))
    //        .check(matches(isDisplayed()))
//
    //}
}


