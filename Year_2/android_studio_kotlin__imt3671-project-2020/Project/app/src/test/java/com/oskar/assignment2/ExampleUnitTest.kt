package com.oskar.assignment2

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.oskar.assignment2.data.AppDatabase
import com.oskar.assignment2.data.Trips
import com.oskar.assignment2.data.TripsDao
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    //Database variables
    private lateinit var tripsDao: TripsDao
    private lateinit var db: AppDatabase
    private var id = 1

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().context
        db = Room.databaseBuilder(context.applicationContext,
            AppDatabase::class.java, "test_user_database"
        )
            .allowMainThreadQueries()
            .build()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    /**
     * Checks database functions. Add, query, delete
     */
    //@Test
    //fun databaseFunctions() {
    //    //User info
    //    val firstName = "Ola"
    //    val lastName = "Nordmann"
//
    //    val userList = arrayListOf(Trips(id, firstName, lastName))
    //    db.tripsDao().insertOne(userList)
    //    id++
//
    //    //Retrieving user
    //    val byName = tripsDao.findByName(firstName, lastName)
//
    //    Assert.assertEquals(byName.firstName, firstName)
    //    Assert.assertEquals(byName.lastName, lastName)
//
    //    //Deleting user
    //    tripsDao.delete(byName)
//
    //    //Trying to retrieve deleted user
    //    val byNameAfterDelete = tripsDao.findByName(firstName, lastName)
//
    //    Assert.assertEquals(byNameAfterDelete.firstName, "")
    //    Assert.assertEquals(byNameAfterDelete.lastName, "")
    //}
}
