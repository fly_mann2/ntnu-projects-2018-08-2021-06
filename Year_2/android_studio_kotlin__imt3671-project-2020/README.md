# Trip Tracker

## Members 

*  Esben Lomholt Bjarnason
*  ------------
*  ------------


## Description 

Trip Tracker is an app that keeps track of walking/hiking trips you have had. You can see the start and end adresses of your trip as well as 
the distance of the trip and google map's approximation of how long it will take. There is also a step counter and you can get the weather in a location you search for 
so you can see what the weather is like where you are going and see if you need to bring a coat or maybe just stay home. Your trips, as well as everyone else's are uploaded 
to a public database so you can see where other people are hiking and maybe get some inspiration.

The app works in both portrait and landcape mode. For the database we used both room and firebase. The room database is a local database that stores your personal trips, 
while firebase is used for global trips data.

As the UI was made for a pixel 2 emulator the UI might look worse or not fit properly depending on your phone.

# Usage

## Trips

When you enter the map view it first ask you to allow the app getting your location. If you click yes, the map will use your location on the map. If you are using an emulator your location will default to the google headquarters in California.
You may have to exit and enter the map view after allowing position for the map to go there. You can search for any location as well and the map will go there.
The way you make a trip and add it to your saved trips as well as the public database is by going to the map view, and pressing the "Make Route" button.
After that you can click two points on the map and the app will make the shortest route to get there for you. Clicking "Save Route" will add the route to your personal trips and the public ones.
Your trips will show up in the "Home" view and the public trips will show up in the "All trips" view you can navigate to using the bottom navigation bar. 
You may have to click the trips button twice or go back to a different view for the data to show up. .

## Weather

In the weather view simply search for your closest city, or any city, and you will get the relevant weather data.
The API used is http://api.openweathermap.org/data/2.5/

## Settings

In the settings menu you access from the cogwheel in the top left corner you can choose to use the app in dark mode if you find that easier on the eyes, or just better looking.
Here you can also delete your saved trips.

## Step counter 

If your phone has a step counter sensor, the steps you have taken while using the app will show up under the settings button near the top of the screen.
If you are using an emulator, or a phone without the sensor, the counter will not show up. 





