package utils

import (
	"context"
	"time"

	"cloud.google.com/go/firestore"
)

//WebhookReg - Used to receive payload from POST request
type WebhookReg struct {
	Event string `json:"event"`
	URL   string `json:"url"`
}

//Webhook - Used to hold webhooks
type Webhook struct {
	ID    string `json:"id"`
	Event string `json:"event"`
	URL   string `json:"url"`
	Time  string `json:"time"`
}

//Invocation - Used as payload when webhook has been invoked
type Invocation struct {
	Event  string    `json:"event"`
	Params []string  `json:"params"`
	Time   time.Time `json:"time"`
}

//Database - Interface for the database
type Database interface {
	Init() error
	Close()
	Save(*Webhook) error
	Delete(*Webhook) error
	ReadByID(string) (Webhook, error)
	ReadAll() ([]Webhook, error)
	SaveTrack(*TopTrack) error
	DeleteTrack(*TopTrack) error
	ReadAllTracks() ([]TopTrack, error)
}

//FirestoreDatabase - Implements database access through Firestore
type FirestoreDatabase struct {
	ProjectID      string
	CollectionName string
	Ctx            context.Context
	Client         *firestore.Client
}

//TopTrack - Used to hold the 5 top tracks in the world (for webhooks)
type TopTrack struct {
	ID        string
	Name      string
	Listeners string
	Artist    string
}

//TopTrackReg - Used to consume JSON from API request
type TopTrackReg struct {
	Tracks struct {
		Track []struct {
			Name      string `json:"name"`
			Listeners string `json:"listeners"`
			URL       string `json:"url"`
			Artist    struct {
				Name string `json:"name"`
			} `json:"artist"`
		} `json:"track"`
	} `json:"tracks"`
}

//TTrackInvocation - Used as payload when toptrack webhook has been invoked
type TTrackInvocation struct {
	Event     string `json:"event"`
	Name      string
	Listeners string
	Artist    string
	Time      time.Time `json:"time"`
}

//Status - gets basic info about project
type Status struct {	  
	Lastfm  int       `json:"lastfm"`
	Mourits int
	DB      int       `json:"database"`
	Version string    `json:"version"`
	Uptime  float64   `json:"uptime"`
	Time    time.Time `json:"time"`
}

//TopArtist - Used for top artist country and tag
type TopArtist struct {
	Topartists struct {
		Artist []struct {
			Name      string `json:"name"`
			Listeners string `json:"listeners"`
			URL       string `json:"url"`
		} `json:"artist"`
	} `json:"topartists"`
}

//ArtistInfo - used for basic info about artist
type ArtistInfo struct {
	Artist struct {
		Name  string `json:"name"`
		URL   string `json:"url"`
		Stats struct {
			Listeners string `json:"listeners"`
			Playcount string `json:"playcount"`
		} `json:"stats"`
		Bio struct {
			Summary string `json:"summary"`
		} `json:"bio"`
	} `json:"artist"`
}

//TrackInfo - info about a track
type TrackInfo struct {
	Track struct {
		Name      string `json:"name"`
		URL       string `json:"url"`
		Duration  string `json:"duration"`
		Listeners string `json:"listeners"`
		Playcount string `json:"playcount"`
		Album     struct {
			Artist string `json:"artist"`
			Title  string `json:"title"`
			URL    string `json:"url"`
		}
	} `json:"track"`
	Lyric struct {
		Text string
	}
}

//LyricInfo - Retrieves lyric text from "mouritz lyrics api"
type LyricInfo struct {
	Result struct {
		Lyrics string `json:"lyrics"`
	} `json:"result"`
}

//TagTop - used for list of top tags
type TagTop struct {
	Toptags struct {
		Tag []struct {
			Count int    `json:"count"`
			Name  string `json:"name"`
			Reach int    `json:"reach"`
		}
	}
}

//TagInfo - info about a tag
type TagInfo struct {
	Tag struct {
		Name  string `json:"name"`
		Total int    `json:"total"`
		Reach int    `json:"reach"`
		Wiki  struct {
			Summary string `json:"summary"`
			Content string `json:"content"`
		}
	}
}

//TopTracks -
type TopTracks struct {
	Tracks struct {
		Track []struct {
			Name      string `json:"name"`
			Listeners string `json:"listeners"`
			URL       string `json:"url"`
			Artist    struct {
				Name string `json:"name"`
				URL  string `json:"url"`
			} `json:"artist"`
		} `json:"track"`
	} `json:"tracks"`
}

//Error - if there is an error in user input ex. country does not exist
type Error struct {
	Error   int    `json:"error"`
	Message string `json:"message"`
}
