package utils

import (
	"context"
	"fmt"

	"cloud.google.com/go/firestore"
	"github.com/pkg/errors"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

//Init initialised the DB.
func (db *FirestoreDatabase) Init() error {
	db.Ctx = context.Background()
	sa := option.WithCredentialsFile("./firebasekey.json")
	var err error
	db.Client, err = firestore.NewClient(db.Ctx, db.ProjectID, sa)
	if err != nil {
		fmt.Printf("Error in FirebaseDatabase.Init() function: %v\n", err)
		return errors.Wrap(err, "Error in FirebaseDatabase.Init()")
	}
	return nil
}

//Close closes the DB connection
func (db *FirestoreDatabase) Close() {
	db.Client.Close()
}

//	WEBHOOKS

//Save saves a webhook into the DB.
func (db *FirestoreDatabase) Save(wh *Webhook) error {
	ref := db.Client.Collection(db.CollectionName).NewDoc()
	wh.ID = ref.ID
	_, err := ref.Set(db.Ctx, wh)
	if err != nil {
		fmt.Println("ERROR saving webhook to Firestore DB: ", err)
		return errors.Wrap(err, "Error in FirebaseDatabase.Save()")
	}
	return nil
}

//Delete deletes a webhook from the DB.
func (db *FirestoreDatabase) Delete(id string) error {
	docRef := db.Client.Collection(db.CollectionName).Doc(id)
	_, err := docRef.Delete(db.Ctx)
	if err != nil {
		fmt.Printf("ERROR deleting webhook with ID (%v) from Firestore DB: %v\n", id, err)
		return errors.Wrap(err, "Error in FirebaseDatabase.Delete()")
	}
	return nil
}

//ReadByID reads webhook by ID from the DB.
func (db *FirestoreDatabase) ReadByID(id string) (Webhook, error) {
	res := Webhook{}
	ref, err := db.Client.Collection(db.CollectionName).Doc(id).Get(db.Ctx)
	if err != nil {
		return res, err
	}
	err = ref.DataTo(&res)
	if err != nil {
		return res, err
	}
	return res, nil
}

//ReadAll reads all webhooks from the DB.
func (db *FirestoreDatabase) ReadAll() ([]Webhook, error) {
	res := Webhook{}
	var sliceRes []Webhook

	//Goes through all documents and appends them to sliceRes
	iter := db.Client.Collection(db.CollectionName).Documents(db.Ctx)
	for {
		ref, err := iter.Next()
		if err == iterator.Done {
			break
		}
		err = ref.DataTo(&res)
		if err != nil {
			return sliceRes, err
		}
		sliceRes = append(sliceRes, res)
	}
	return sliceRes, nil
}

// TOP TRACKS

//ReadAllTracks reads all top tracks from the DB.
func (db *FirestoreDatabase) ReadAllTracks() ([]TopTrack, error) {
	res := TopTrack{}
	var sliceRes []TopTrack

	//Goes through all documents and appends them to sliceRes
	iter := db.Client.Collection(db.CollectionName).Documents(db.Ctx)
	for {
		ref, err := iter.Next()
		if err == iterator.Done {
			break
		}
		err = ref.DataTo(&res)
		if err != nil {
			return sliceRes, err
		}
		sliceRes = append(sliceRes, res)
	}
	return sliceRes, nil
}

//SaveTrack saves a toptrack into the DB.
func (db *FirestoreDatabase) SaveTrack(tt *TopTrack) error {
	ref := db.Client.Collection(db.CollectionName).NewDoc()
	tt.ID = ref.ID
	_, err := ref.Set(db.Ctx, tt)
	if err != nil {
		fmt.Println("ERROR saving webhook to Firestore DB: ", err)
		return errors.Wrap(err, "Error in FirebaseDatabase.Save()")
	}
	return nil
}

//DeleteTrack deletes a top track from the DB.
func (db *FirestoreDatabase) DeleteTrack(id string) error {
	docRef := db.Client.Collection(db.CollectionName).Doc(id)
	_, err := docRef.Delete(db.Ctx)
	if err != nil {
		fmt.Printf("ERROR deleting webhook with ID (%v) from Firestore DB: %v\n", id, err)
		return errors.Wrap(err, "Error in FirebaseDatabase.Delete()")
	}
	return nil
}
