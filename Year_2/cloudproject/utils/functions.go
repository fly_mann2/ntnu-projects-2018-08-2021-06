package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
)

//GetDuration - Translates ms from api into minutes
func GetDuration(dur string) string {
	var duration float64
	var minutes int
	var seconds int

	duration, _ = strconv.ParseFloat(dur, 4)
	minutes = int(duration / 60000)
	seconds = int(((duration / 60000) - float64(minutes)) * 60)

	return strconv.Itoa(minutes) + ":" + strconv.Itoa(seconds)
}

//GetKey - Checks the arguments the user writes in url, adds appropriate char in front
func GetKey(r *http.Request, _key string) string {
	u := r.FormValue(_key)
	//Special case for limit
	if strings.Contains(_key, "limit") && u == "" {
		u = "&" + _key + "=" + "5" //default = 5
	} else if u == "" { // no key
		u = "" // default
	} else { //&
		if strings.Contains(_key, "method") { //Method always first, needs ? in front
			u = "?" + _key + "=" + u
		} else {
			u = "&" + _key + "=" + u
		}
	}
	return u
}

//GetLastAPI - Gets data and decodes/encodes into struct
func GetLastAPI(w http.ResponseWriter, genreURL string, g interface{}) {

	var e Error

	resp1, errAns1 := http.Get(genreURL)
	if errAns1 != nil {
		http.Error(w, "Could not get data", http.StatusNotFound)
	}

	errAns1 = json.NewDecoder(resp1.Body).Decode(&e)
	w.Header().Add("content-type", "application/json")

	//If user writes something wrong- outputs the error struct
	if e.Error != 0 {
		errAns1 = json.NewEncoder(w).Encode(e)
	} else {
		resp, errAns := http.Get(genreURL)
		if errAns != nil {
			http.Error(w, "Could not get data", http.StatusNotFound)
		}

		errAns = json.NewDecoder(resp.Body).Decode(&g)
		if errAns != nil {
			http.Error(w, "Could not decode", http.StatusBadRequest)
		}
		w.Header().Add("content-type", "application/json")
		errAns = json.NewEncoder(w).Encode(g)
		if errAns != nil {
			http.Error(w, "Could not encode", http.StatusBadRequest)
		}
	}

}

//GetIDWebhook - Returns a specific webhook
func GetIDWebhook(w http.ResponseWriter, id string) (Webhook, error) {
	wh, err := DbWh.ReadByID(id)
	if err != nil {
		fmt.Println("Error reading webhook by ID: ", err)
	}
	return wh, err
}

//GetAllWebhooks - Returns all webhooks
func GetAllWebhooks() []Webhook {
	sliceOfAllWebhooks, err := DbWh.ReadAll()
	if err != nil {
		fmt.Println("Error reading all webhooks: ", err)
	}
	return sliceOfAllWebhooks
}

//GetAllToptracks - Returns all top tracks
func GetAllToptracks() []TopTrack {
	sliceOfAllToptracks, err := DbTt.ReadAllTracks()
	if err != nil {
		fmt.Println("Error reading all webhooks: ", err)
	}
	return sliceOfAllToptracks
}

//EqualSlices - Checks two slices if they contain the same elements
func EqualSlices(t, dbt []TopTrack) (bool, TopTrack) {
	var diffT TopTrack

	//Sorts dbt in ascending order
	sort.Slice(dbt, func(i, j int) bool {
		return dbt[i].Listeners > dbt[j].Listeners
	})

	//Sorts t in ascending order
	sort.Slice(t, func(i, j int) bool {
		return t[i].Listeners > t[j].Listeners
	})

	//Checks if different length
	if len(t) != len(dbt) {
		return false, diffT
	}

	//Checks every element
	for i, v := range t {
		if (v.Name != dbt[i].Name) || (v.Artist != dbt[i].Artist) || (v.Listeners != dbt[i].Listeners) {
			diffT = v
			return false, diffT
		}
	}
	return true, diffT
}
