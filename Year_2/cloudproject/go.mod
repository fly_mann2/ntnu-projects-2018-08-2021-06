module lastfm

go 1.13.1

require (
	cloud.google.com/go/firestore v1.0.0
	github.com/pkg/errors v0.8.1
	google.golang.org/api v0.13.0
)
