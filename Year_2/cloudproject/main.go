package main

import (
	"fmt"
	"lastfm/handlers"
	"lastfm/utils"

	"log"
	"net/http"
	"os"
	"time"
)

//timeInit - starts timer for status
func timeInit() {
	handlers.StartTime = time.Now()
}

//A function that activates every 30 minutes after application's start. Used to update database (because of webhooks)
func checkForEvery30min() {
	ticker := time.NewTicker(15 * time.Minute)
	defer ticker.Stop()
	done := make(chan bool)
	for {
		select {
		case <-done:
			ticker.Stop()
			fmt.Println("Done!")
			return
		case t := <-ticker.C:
			handlers.UpdateTopTracks()
			fmt.Println("Database updated at: ", t)
		}
	}
}

//HandlerNil if you dont write anyting after localhost/ip
func HandlerNil(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Default Handler: Invalid request received.")
	http.Error(w, "Invalid request", http.StatusBadRequest)
}

func main() {
	timeInit()
	//Init database connection to webhook collection
	utils.DbWh = utils.FirestoreDatabase{ProjectID: utils.ProjectID, CollectionName: utils.CollectionWebhooks}
	err := utils.DbWh.Init()
	if err != nil {
		log.Fatal(err)
	}
	defer utils.DbWh.Close()

	//Init database connection to toptracks collection
	utils.DbTt = utils.FirestoreDatabase{ProjectID: utils.ProjectID, CollectionName: utils.CollectionToptracks}
	err = utils.DbTt.Init()
	if err != nil {
		log.Fatal(err)
	}
	defer utils.DbTt.Close()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	http.HandleFunc("/", HandlerNil)
	http.HandleFunc("/lastfm/v1/status", handlers.StatusHandler)
	http.HandleFunc("/lastfm/v1/music/", handlers.MusicHandler)
	http.HandleFunc("/lastfm/v1/webhooks/", handlers.WebhookHandler)
	fmt.Println("Listening on port " + port)

	go checkForEvery30min()

	log.Fatal(http.ListenAndServe(":"+port, nil))
}
