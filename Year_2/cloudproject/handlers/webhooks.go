package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"lastfm/utils"
	"net/http"
	"strconv"
	"strings"
	"time"
)

//WebhookHandler - handles create, read and delete on webhooks
func WebhookHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		// Expects incoming body in terms of WebhookRegistration struct
		var whReg utils.WebhookReg
		err := json.NewDecoder(r.Body).Decode(&whReg)
		if err != nil {
			fmt.Println(err)
			http.Error(w, "Error: Could not decode JSON-encoded data", http.StatusBadRequest)
		}

		//Populates webhook struct
		wh := utils.Webhook{
			//ID from database
			Event: whReg.Event,
			URL:   whReg.URL,
			Time:  time.Now().Format(time.RFC850),
		}

		//Saves the webhook to database
		fmt.Println("Webhook " + whReg.URL + " has been registered.")
		err = utils.DbWh.Save(&wh)
		if err != nil {
			fmt.Println("Error saving webhook: ", err)
		}

		//Returns identifier received from database
		fmt.Fprintf(w, "Your webhook's ID is: %v", wh.ID)

	case http.MethodGet:
		http.Header.Add(w.Header(), "content-type", "application/json")
		//Check if on the right endpoint
		parts := strings.Split(r.URL.Path, "/")
		if len(parts) < 4 {
			http.Error(w, "Error: Incorrect path given", http.StatusBadRequest)
			return
		}
		//Extracts id from endpoint
		id := parts[4]

		//Check if ID is set or not
		if id != "" {
			//Specific ID requested
			wh, err := utils.GetIDWebhook(w, id)
			if err != nil {
				http.Error(w, "Error: Could not find webhook with that ID", http.StatusBadRequest)
				return
			}
			//Encode webhook to json and send it as response
			err = json.NewEncoder(w).Encode(wh)
			if err != nil {
				fmt.Println(err)
				http.Error(w, "Error: Could not encode JSON-encoded data", http.StatusInternalServerError)
			}
		} else {
			//Prints all webhooks
			err := json.NewEncoder(w).Encode(utils.GetAllWebhooks())
			if err != nil {
				fmt.Println(err)
				http.Error(w, "Error: Could not encode JSON-encoded data", http.StatusInternalServerError)
			}
		}
	case http.MethodDelete:
		//Check if on the right endpoint
		parts := strings.Split(r.URL.Path, "/")
		if len(parts) < 4 {
			http.Error(w, "Error: Incorrect path given", http.StatusBadRequest)
			return
		}
		//Extracts id from endpoint
		id := parts[4]

		//Check if ID is set or not
		if id != "" {
			//Specific ID requested
			err := utils.DbWh.Delete(id)
			if err != nil {
				fmt.Println("Error reading webhook by ID: ", err)
			} else {
				fmt.Fprintf(w, "Successfully deleted webhook with id: %v", id)
			}
		} else {
			http.Error(w, "Error: No ID specified in the URL", http.StatusBadRequest)
		}
	default:
		http.Error(w, "Not implemented", http.StatusNotImplemented)
		return
	}
}

//WebhookInvoke - Checks that only webhooks with the specific event gets invoked
func WebhookInvoke(event string, t utils.TopTrack) {
	//Gets all webhooks from database
	allWh := utils.GetAllWebhooks()

	//Supports only POST and GET requests at the moment
	switch event {
	case "NewTop5Track":
		fmt.Println("Received NewTop5Track request...")

		//Constructs payload
		payloadTrack := utils.TTrackInvocation{
			Event:     event,
			Name:      t.Name,
			Artist:    t.Artist,
			Listeners: t.Listeners,
			Time:      time.Now(),
		}

		//Iterates through webhooks to find the correct webhooks
		for _, v := range allWh {
			if v.Event == "NewTop5Track" {
				//Invokes webhook
				go CallURL(v.URL, payloadTrack)
			}
		}

	default:
		fmt.Println("Invalid method was invoked (webhooks)")
	}
}

//CallURL - Invokes all webhooks with that event
func CallURL(url string, payload utils.TTrackInvocation) {
	fmt.Println("Attempting invocation of url " + url + " ...")

	//Marshals payload to []byte
	reqBody, err := json.Marshal(&payload)
	if err != nil {
		fmt.Println("Error: Could not marshal data", err)
	}

	//Sends POST request to webhook registered URL
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(reqBody))
	if err != nil {
		fmt.Println("Error in HTTP request: " + err.Error())
	}
	defer resp.Body.Close()

	//Gets feedback from the webhook
	fmt.Println("Webhook invoked. Received status code " + strconv.Itoa(resp.StatusCode))
}

//UpdateTopTracks - Updates the top tracks in db, if needed and invokes webhooks
func UpdateTopTracks() {
	var tracksReg utils.TopTrackReg
	var tracks []utils.TopTrack

	//Sends GET request to top tracks chart
	url := utils.APIURL + "chart.gettoptracks&api_key=" + utils.APIKEY + "&format=json&limit=5"
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Error: Could not issue a GET request to top tracks chart", err)
	}
	defer resp.Body.Close()

	//Decode body into struct
	err = json.NewDecoder(resp.Body).Decode(&tracksReg)
	if err != nil {
		fmt.Println(err)
	}

	//Retrieve only needed fields
	for _, v := range tracksReg.Tracks.Track {
		track := utils.TopTrack{
			Name:      v.Name,
			Listeners: v.Listeners,
			Artist:    v.Artist.Name,
		}
		tracks = append(tracks, track)
	}

	//Gets old top tracks from db
	tracksDb := utils.GetAllToptracks()

	//Checks if there is any changes
	equal, t := utils.EqualSlices(tracks, tracksDb)
	//If different then proceed with updating the db and invoke potential webhooks
	if equal {
		return
	}

	//Invokes webhooks
	WebhookInvoke("NewTop5Track", t)

	//Deleting all top tracks currently in the database
	for _, v := range tracksDb {
		err := utils.DbTt.DeleteTrack(v.ID)
		if err != nil {
			fmt.Println("Error deleting track by ID: ", err)
		}
	}
	//Adding in the newly updated top tracks
	for _, v := range tracks {
		err = utils.DbTt.SaveTrack(&v)
		if err != nil {
			fmt.Println("Error saving webhook: ", err)
		}
	}
}
