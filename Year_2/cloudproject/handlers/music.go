package handlers

import (
	"fmt"
	"lastfm/utils"
	"net/http"
	"strings"
)

//MusicHandler - gets all different types of basic info from lastfm
func MusicHandler(w http.ResponseWriter, r *http.Request) {

	parts := strings.Split(r.URL.Path, "/")
	meth := parts[4] // part[3] = music

	//Last fm method
	methodR := []string{
		"tag.getinfo", "tag.gettopartists", "tag.getTopTags", "tag.gettoptracks",
		"track.getInfo",
		"artist.getInfo",
		"geo.gettopartists", "geo.gettoptracks",
	}
	//Shortcut methods
	methodX := []string{
		"tag", "tag_top_artist", "tag_top", "tag_top_tracks",
		"track",
		"artist",
		"artist_top_country", "track_top_country"}

	//Translates method
	for i := 0; i < len(methodX); i++ {
		if meth == methodX[i] {
			meth = methodR[i]
			fmt.Println("meth:" + meth)

		}
	}

	URL := utils.APIURL + meth + utils.GetKey(r, "tag") + utils.GetKey(r, "artist") +
		utils.GetKey(r, "country") + utils.GetKey(r, "track") + utils.GetKey(r, "limit") + "&api_key=" + utils.APIKEY + "&format=json"

	fmt.Println(URL)
	//Switch on the method, makes appropriate structs for each method.
	switch meth {
	case "tag.getinfo":
		{
			var t utils.TagInfo
			utils.GetLastAPI(w, URL, &t)

		}
	case "track.getInfo":
		{
			TrackHandler(w, r)

		}
	case "tag.gettopartists":
		{
			var t utils.TopArtist
			utils.GetLastAPI(w, URL, &t)

		}
	case "tag.getTopTags":
		{
			var t utils.TagTop
			utils.GetLastAPI(w, URL, &t)

		}
	case "artist.getInfo":
		{
			var t utils.ArtistInfo
			utils.GetLastAPI(w, URL, &t)

		}
	case "geo.gettopartists":
		{
			var t utils.TopArtist
			utils.GetLastAPI(w, URL, &t)

		}
	case "geo.gettoptracks":
		{
			var t utils.TopTracks
			utils.GetLastAPI(w, URL, &t)

		}
	case "tag.gettoptracks":
		{
			var t utils.TopTracks
			utils.GetLastAPI(w, URL, &t)
		}

	}
}
