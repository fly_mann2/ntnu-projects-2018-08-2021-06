package handlers

import (
	"encoding/json"
	"lastfm/utils"
	"net/http"
)

//TrackHandler handles everything to do with retrieving information from
//tracks users search for. Searching for artist name and track title is
//mandatory for this script to go through. Retreieves name, url, track
//duration, album and lyric text of said track
func TrackHandler(w http.ResponseWriter, r *http.Request) {

	// Declares the two structs we will be using throughout this handler:
	//		--> trackinfo and lyric text
	var trackInfo utils.TrackInfo
	var lyricInfo utils.LyricInfo

	// Gets the attribute from the user-typed url
	attri := r.URL.Query()

	// gets the track title and artist
	track := attri.Get("track")
	artist := attri.Get("artist")

	// creates the url we want to use
	path := "http://ws.audioscrobbler.com/2.0/"
	query := "?method=track.getInfo&api_key=7f39fb92f00358e621eddf7368040df3&artist=" + artist + "&track=" + track + "&format=json"

	// retrieves the endpoint from the urls we use to lastfm and mourits lyrics
	trackURL := path + query
	lyricsURL := "https://mourits-lyrics.p.rapidapi.com/?artist=" + artist + "&song=" + track

	// gets the response from lastfm and returns an error if no response from server
	resp, err := http.Get(trackURL)
	if err != nil {
		http.Error(w, "Could not get data", http.StatusNotFound)
	}

	defer resp.Body.Close()

	// Decodes the content on to our trackinfo struct
	_ = json.NewDecoder(resp.Body).Decode(&trackInfo)

	// turns content of the struct into json format
	w.Header().Add("content-type", "application/json")

	// the duration from lastfm tracks are set to milliseconds.
	// Created a function which turns milliseconds to minutes
	trackInfo.Track.Duration = utils.GetDuration(trackInfo.Track.Duration)

	// Creates a request for permission to access information from mourits lyrics
	req, _ := http.NewRequest("GET", lyricsURL, nil)

	// host and api permission and key
	req.Header.Add("x-rapidapi-key", "4de6d276aemshf0d4b72ac1abcbdp1b154bjsn9c5795afc216")

	// gets the response from server
	res, _ := http.DefaultClient.Do(req)

	// Decodes the content on to our lyric text struct
	_ = json.NewDecoder(res.Body).Decode(&lyricInfo)

	// puts content from lyric struct into trackinfo struct
	trackInfo.Lyric.Text = lyricInfo.Result.Lyrics

	// encodes trackinfo into server
	err = json.NewEncoder(w).Encode(trackInfo)
	if err != nil {
		http.Error(w, "Could not encode", http.StatusBadRequest)
	}
	defer res.Body.Close()

}
