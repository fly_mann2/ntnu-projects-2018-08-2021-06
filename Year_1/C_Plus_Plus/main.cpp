///////////////////////// INCLUDES ////////////////////////////

#include <iostream>
#include <fstream>
#include <cctype>
#include <cstring>

#include "funksjoner.h"
#include "Kunder.h"
#include "Steder.h"
#include "Arrangementer.h"



using namespace std;

Kunder kundeBase;
Steder stedBase;
Arrangementer arrBase;

// Styrer hele butikken

int main() {

	
	kundeBase.lesFraFil();
	stedBase.lesFraFil();
	arrBase.lesFraFil();

	char valg;
	skrivMeny();
	valg = les();

	while (valg != 'Q')
	{

		switch (valg)
		{
		case 'K':				//Kunde-valg
			kundeBase.valg();
			break;
		case 'S':				//Sted-valg
			stedBase.valg(valg);
			break;
		case 'O':				//Oppsett-valg
			stedBase.valg(valg);
			break;
		case 'A':				//Arrangement-valg
			arrBase.valg();
			break;
		default:
			skrivMeny();
		}
		valg = les();

	}

	kundeBase.skrivTilFil();
	stedBase.skrivTilfil();
	arrBase.skrivTilFil();


	


	return 0;
}

