#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif 

using namespace std;
#include "Kunde.h"
#include "funksjoner.h"
#include "Kunder.h"
#include <iostream>
#include "CONST.h"
#include <cstring>

Kunde::Kunde()
{

}

Kunde::~Kunde()
{
	delete[] navn;
	delete[] adresse;
	delete[] poststed;
	delete[] epost;
}
										
Kunde::Kunde(int nr) : NumElement(nr)	//Leser inn kundens data med kundenr
{										//som parameter. Sendes opp til 
	kundenr = nr;						//NumElement for � bli sortert

	char na[STRLEN];
	std::cout << "\n\nNavn: "; std::cin.getline(na, STRLEN);
	navn = new char[strlen(na) + 1];
	strcpy(navn, na);

	char adr[STRLEN];
	cout << "Adresse: "; std::cin.getline(adr, STRLEN);
	adresse = new char[strlen(adr) + 1];
	strcpy(adresse, adr);

	char sted[STRLEN];
	cout << "Poststed: "; cin.getline(sted, STRLEN);
	poststed = new char[strlen(sted) + 1];
	strcpy(poststed, sted);

	postnr = les("Postnr: ", 0000, 9999);

	tlf = les("Telefonnr: ", 11111111, 99999999);

	char mail[STRLEN];
	cout << "Epost: "; cin.getline(mail, STRLEN);
	epost = new char[strlen(mail) + 1];
	strcpy(epost, mail);
}

Kunde::Kunde(int nr, ifstream & inn) : NumElement(nr) //Leser kundens data
{													  //fra fil
	kundenr = nr;

	char na[STRLEN];
	inn.getline(na, STRLEN);
	navn = new char[strlen(na) + 1];
	strcpy(navn, na);

	char adr[STRLEN];
	inn.getline(adr,STRLEN);
	adresse = new char[strlen(adr) + 1];
	strcpy(adresse, adr);

	char sted[STRLEN];
	inn.getline(sted, STRLEN);
	poststed = new char[strlen(sted) + 1];
	strcpy(poststed, sted);


	inn >> postnr;
	inn.ignore();

	inn >> tlf;
	inn.ignore();

	char mail[STRLEN];
	inn >> mail;
	inn.ignore();
	epost = new char[strlen(mail) + 1];
	strcpy(epost, mail);

}


void Kunde::display()		//Displayer kundens data
{
	cout << "\n\nKundenr: " << number << '\n';
	cout << "Navn: " << navn << '\n';
	cout << "Adresse: " << adresse << ", " << postnr << ", " << poststed
		<< '\n';
	cout << "Telefon: " << tlf << '\n'; 
	cout << "Epost: " << epost << '\n';
 
}



void Kunde::skrivTilFil(ofstream & ut)	//Skriver kundens data til fil
{

	ut << kundenr << '\n' << navn << '\n';
	ut << adresse << '\n' << poststed << '\n';
	ut << postnr << '\n' << tlf << '\n' << epost << '\n';
	

}

void Kunde::compNavn(char* n)	//Sammenlikner kundens navn med det som 
{								//blir sendt med som parameter.
	if (strstr(navn, n) != 0)	//Displayer den kundens data hvis de er like
	{
		display();
	}
	
}

							   //Skriver til biletter.dta
void Kunde::skrivTilBill(ofstream & ut)
{
	ut << "\n\nKundenr: " << number << '\n';
	ut << "Navn: " << navn << '\n';
	ut << "Adresse: " << adresse << ", " << postnr << ", " << poststed
		 << '\n';
	ut << "Telefon: " << tlf << '\n';
	ut << "Epost: " << epost << '\n';
}