#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif 


#include "Kunde.h"
#include "Kunder.h"
#include "CONST.h"
#include "funksjoner.h"
#include <iostream>


Kunder::Kunder()
{
	kunder = new List(Sorted);	//Lager en ny sortert liste med kunder
}

Kunder::~Kunder()
{
	delete kunder;
}

void Kunder::valg()
{
	char ans;
	cin >> ans; ans = toupper(ans); cin.ignore();
	char valg[STRLEN];

	switch (ans)
	{
	case 'D':
		cout << "\nHvilke kunder vil du se? (nr,navn, blank=alle) ";
		cin.getline(valg, STRLEN);
		if (kunder->inList(atoi(valg))) //Gj�r om det bruker skriver til int
		{								//og ser om en kunde har dette kundenr
			display(atoi(valg));
		}
		else if (strlen(valg) > 3 )		//Hvis teksten er lengere enn 3
		{								//sjekkes det om kunder har dette som
										//navn
			for (int i = 1; i <= antKunder; i++)
			{
				Kunde* temp = (Kunde*)kunder->removeNo(i);
				temp->compNavn(valg);
				kunder->add(temp);
			}

		}
		else if (strlen(valg) == 0)		//Hvis bruker ikke skriver noe 
		{								//displayes alle kunder i listen
			display();
		}
		else
		{
			cout << "\nIngen kunder stemmer med soket ditt\n";
		}
		break;
	case 'N':
		nyKunde();
		break;
	case 'E':
		endreKunde();
		break;
	default: break;
	}

}



void Kunder::display()	//Displayer hele listen
{
	kunder->displayList();
}

void Kunder::display(int n)	//Displayer en kunde med ett kundenr
{
	kunder->displayElement(n);
}



void Kunder::lesFraFil()
{
	ifstream inn("Kunder.dta");
	Kunde* nyKunde;
	if (inn)
	{
		inn >> antKunder; inn.ignore(); //Leser f�rst inn antall kunder
		cout << "leser fra filen Kunder.dta\n";
		int nr;
		for (int i = 1; i <= antKunder; i++) //Leser inn alle kundene
		{
			inn >> nr; inn.ignore();
			nyKunde = new Kunde(nr, inn);
			kunder->add(nyKunde);
		}
	}
	else
		cout << "Finner ikke filen Kunder.dta";
}

void Kunder::nyKunde() //Legger til en ny kunde i listen
{
	Kunde* nyKunde;
	nyKunde = new Kunde(++antKunder);
	kunder->add(nyKunde);
}

void Kunder::endreKunde()	//Sletter kunden og lager en ny kunde med samme nr
{
	int nr = les("Hvilken kunde vil du endre p�? (nr)" ,1, antKunder);
	kunder->destroy(nr);
	kunder->add(new Kunde(nr));
}

void Kunder::skrivTilFil() //Skriver alle kundene til fil
{
	
	ofstream ut("Kunder.dta");

	ut << antKunder << '\n'; //Skriver f�rst antall kunder
	cout << "Skriver til filen Kunder.dta";
	
	for (int i = 1; i <= antKunder; i++)	//SKriver alle kundene
	{
		Kunde* temp = (Kunde*)kunder->removeNo(i);
		temp->skrivTilFil(ut);
		kunder->add(temp);

	}


}
int Kunder::getAntKunder() { //Returnerer antall kunder
	return antKunder;
}

							//Skriver til biletter.dta
void Kunder::skrivTilBill(ofstream & ut, int nr)
{

	Kunde* temp = (Kunde*)kunder->removeNo(nr);
	temp->skrivTilBill(ut);
	kunder->add(temp);

}