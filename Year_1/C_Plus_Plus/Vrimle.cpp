#include "Sone.h"
#include "funksjoner.h"
#include "Vrimle.h"
#include <iostream>

extern Kunder kundeBase;

Vrimle::~Vrimle()
{
	delete[] billetter;
}
					//Leser inn sonens data
Vrimle::Vrimle(char* navn) : Sone(navn) {

	int antBilletter = les("Antall billetter: ", 10, 10000);

	billetter = new int[antBilletter + 1];
	for (int i = 1; i <= antBilletter; i++)
		billetter[i] = 0;

	setAntb(1, antBilletter);

	setType('V');
}
					//Leser inn sonens data fra fil
Vrimle::Vrimle(char* navn, ifstream & inn) : Sone(navn, inn)
{
	setType('V');

	int antBilletter = getAntallBilletter();

	billetter = new int[antBilletter + 1];
	for (int i = 1; i <= antBilletter; i++)
		billetter[i] = 0;

}

					//Skriver til steder.dta
void Vrimle::skrivTilFil(ofstream & ut)
{

	ut << 'V' << '\n';
	Sone::skrivTilFil(ut);

}
					//Skriver til ARR_nr.dta
void Vrimle::skrivTilArrnr(ofstream & ut)
{
	
	ut << 'V' << '\n';
	Sone::skrivTilArrnr(ut);

	for (int j = 1; j <= getAntallBilletter(); j++)
	{
		ut << billetter[j] << ' ';
	}
	ut << '\n';


}

void Vrimle::display() {

	Sone::display();

}

void Vrimle::kjopBil() {											//Styrer billettkj�p for vrimlesoner

	int kundeNr = 0, antBil = 0;									//Nullstiller variabler
	int billTilOvers = getAntallBilletter() - returnAntSolgt();		//Antall billetter tilgjengelige (etter kj�p)

	display();
	if (returnAntSolgt() < getAntallBilletter()) {					//S� lenge ikke alle billetter er kj�pt opp

		cout << "\nAntall billetter tilgjengelige: " << billTilOvers << '\n';
		kundeNr = les("Kunde nr ditt?", 1, kundeBase.getAntKunder());
		antBil = les("Hvor mange billetter skal du ha?", 1, getAntallBilletter());

		if (antBil <= billTilOvers) {								//S� lenge man ikke bestiller flere billetter enn
																	//billetter tilgjengelige for �yeblikket
			ofstream ut;											//Skriver ut til "BILLETTER.DTA"
			ut.open("BILLETTER.dta", std::ios::app);
			Sone::skrivTilBil(ut);
			
			//Starter fra siste person som kj�pte, og looper til antall billetter bestilt
			//(inkl. siste person som kj�pte)
			for (int i = sisteBillettKjopt + 1; i <= sisteBillettKjopt + antBil; i++)
			{
				billetter[i] = kundeNr;								//Gir billettene til kunden som bestilte (kundenr.)
				bilSolgt();											//Registrer at en billett har blitt solgt
			}
			ut << "Ant Billetter: " << antBil << "\n";
			sisteBillettKjopt += antBil;							//Siste billett som ble solgt, s�rger for at neste kunde
																	//som bestiller blir tildelt riktig plass i arrayen
		}
		else { cout << "Billetter bestilt overskrider antall billetter tilgjengelige!\n"; }

	}
	else { cout << "Ingen flere billetter tilgjengelige!\n"; }

}
void Vrimle::lesFraFil(ifstream & inn) {	//Leser fra ARR_nr.dta
	for (int i = 1; i <= getAntallBilletter(); i++)
	{
		inn >> billetter[i];
		if (billetter[i] >= 1)
		{
			bilSolgt(); // antSolgt++
		}
	}
	inn.ignore();
}
