#if !defined(__Sone_H)
#define  __Sone_H

#include <fstream>
#include "ListTool2B.H"

using namespace std;

class Sone : public TextElement 
{
private:
	char* sonenavn;
	int antBilletter;
	int antSolgt;
	int pris;
	char avType = 'S';

public:
	
	Sone();
	~Sone();
	Sone(char* navn);
	Sone(char* navn, ifstream & inn);
	virtual void skrivTilFil(ofstream & ut);
	virtual void lesFraFil(ifstream & inn);
	virtual void display();
	char getType();
	void setAntb(int rader, int seter);
	void bilSolgt();
	int getAntallBilletter();
	int returnAntSolgt();
	virtual void skrivTilArrnr(ofstream & ut);

	void setType(char t);


	

	virtual void kjopBil();
	void skrivTilBil(ofstream & ut);



};
#endif
