#include "Sted.h"
#include "Steder.h"
#include "funksjoner.h"
#include <iostream>


Steder::Steder() {
	steder = new List(Sorted);
}

Steder::~Steder()
{
	delete steder;
}

void Steder::valg(char valg)
{

	char kommando;
	bool fantSted = false;
	if (valg == 'S')	//Sted kommandoer
	{
		cin >> kommando; kommando = toupper(kommando); cin.ignore();

		switch (kommando) {

		case 'D': display();	//Displayer stedene
			break;


		case 'N': nyttSted();	//Legger til nytt sted	
			break;


		default:  break;
		}
	}
	else if (valg == 'O')	//Oppsett kommandoer
	{
		cin >> kommando; kommando = toupper(kommando); cin.ignore();

		switch (kommando)
		{
		case 'D': //Displayer oppsett p� et sted
			cout << "\nSted: ";
			char navn[STRLEN]; cin.getline(navn, STRLEN);
			
			for (int i = 1; i <= antSteder; i++)
			{
				Sted* temp = (Sted*)steder->removeNo(i);
				if (temp->compareStedNavn(navn))
				{
					fantSted = true;
					int valgX = les("Oppsett nr: ", 1, temp->getAnt());
					temp->displayOppsett(valgX);

				}
				steder->add(temp);
			
			}
			if (fantSted == false)
			{
				cout << "\nDette stedet finnes ikke\n";
			}
			break;
		case 'N':	//Nytt oppsett p� et sted
			cout << "\nSted: ";
			char _navn[STRLEN]; cin.getline(_navn, STRLEN);
			for (int i = 1; i <= antSteder; i++)
			{
				Sted* temp = (Sted*)steder->removeNo(i);
				if (temp->compareStedNavn(_navn))
				{
					fantSted = true;
					if (temp->getAnt() < 5) //Plass til flere oppsett
					{
						temp->nyttOppsett();
					}
					else
					{
						cout << "\nIkke plass til flere oppsett\n";
					}
				}
				steder->add(temp);
			}
			if (fantSted == false)
			{
				cout << "\nDette stedet finnes ikke\n";
			}
			break;
		case 'E': //Endre p� et oppsett
			cout << "\nStedsnavn: ";
			char n[STRLEN]; cin.getline(n, STRLEN);
			for (int i = 1; i <= antSteder; i++)
			{
				Sted* temp = (Sted*)steder->removeNo(i);
				if (temp->compareStedNavn(n))
				{
					fantSted = true;
					if (temp->getAnt() != 0) //Hvis det finnes oppsett
					{
						int nr = les("Hvilket oppsett vil du endre pa? ", 
							1, temp->getAnt());
						temp->endreOppsett(nr);
					}
					else
					{
						cout << "\nIngen oppsett a endre pa\n";
					}
				}
				steder->add(temp);
			}
			if (fantSted == false)
			{
				cout << "\nDette stedet finnes ikke\n";
			}
			break;
		default: 
			break;
		}
	}
}


void Steder::lesFraFil() { //Leser steder fra fil

	ifstream inn("Steder.dta");
	Sted* nyttSted;

	if (inn) {

		char n[STRLEN];
		inn >> antSteder; inn.ignore(); //Leser inn antall steder
		cout << "\nLeser fra fil 'Steder.dta'. . . .\n";

		for (int i = 1; i <= antSteder; i++) { //Leser inn alle stedene
			inn.ignore(); 
			inn.getline(n, STRLEN);
			nyttSted = new Sted(n, inn);
			steder->add(nyttSted);
		}
	}
	else { cout << "\nFinner ikke filen 'Steder.dta'!\n"; }
	
}

void Steder::display() //Displayer hele listen
{
	steder->displayList();
}

void Steder::nyttSted() { //Legger til et nytt sted i listen

	char navn[NAVNLEN];

	cout << "\nSkriv inn stedsnavn: ";
	cin.getline(navn, NAVNLEN); 

	Sted * nyttSted;
	bool fant = false;

	for (int i = 1; i <= antSteder; i++) {

		Sted* temp = (Sted*)steder->removeNo(i);

		if (temp->compareStedNavn(navn)) {

			cout << "Sted med dette navnet finnes allerede!\n";
			fant = true;

		}
		steder->add(temp);
	}

	if (fant == false) //Hvis ingen steder har samme navn
	{
		nyttSted = new Sted(navn);
		steder->add(nyttSted);
		antSteder++;
	}



}

List* Steder::kopier(char* nvn, int nr) { // spor om sted navn og oppsett nr
	List* liste = NULL;
	Sted* sted;

	if ((sted = (Sted*)steder->remove(nvn))) {
		liste = sted->kopier(nr); // kj�rer funksjonen til Stedet med navnet(nvn)
		steder->add(sted);
	}
	return liste;
}

void Steder::skrivTilfil() { //Skriver stedene til fil

	int i;
	ofstream ut("Steder.dta");

	ut << antSteder << '\n';
	cout << "\nSkriver til filen 'Steder.dta'...";

	for (i = 1; i <= antSteder; i++) {

		Sted* temp = (Sted*)steder->removeNo(i);
		temp->skrivTilFil(ut);
		steder->add(temp);
	}

}



					//Returnerer et sted-objekt
Sted* Steder::getSted(char* navn)
{
	Sted* temp = nullptr;

	for (int i = 1; i <= antSteder; i++)
	{
		temp = (Sted*)steder->removeNo(i);

		if (temp->compareStedNavn(navn))
		{
			steder->add(temp);
			return temp;
		}
		steder->add(temp);
		temp = nullptr;
	}

	return temp;
}