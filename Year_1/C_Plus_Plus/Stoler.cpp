#include "Sone.h"
#include "Stoler.h"
#include <iostream>
#include "funksjoner.h"
#include "CONST.h"
#include "Kunder.h"

extern Kunder kundeBase;


Stoler::~Stoler()
{
										// Sletter Billetter
	for (int i = 1; i <= antRader; i++)
		delete billetter[i];
	delete billetter;
}

							//Leser inn sonens data
Stoler::Stoler(char* navn) : Sone(navn)
{
	antRader = les("Antall rader:", 1, MAXRADER);
	antSeter = les("Antall seter: ", 1, MAXSETER);
	setAntb(antRader, antSeter);

	billetter = new int*[antRader + 1];
	for (int i = 1; i <= antRader; i++)
		billetter[i] = new int[antSeter + 1];

	for (int i = 1; i <= antRader; i++)
		for (int j = 1; j <= antSeter; j++)
			billetter[i][j] = 0;
}
							//Leser inn sonens data fra fil
Stoler::Stoler(char* navn, ifstream & inn) : Sone(navn, inn)
{
	
	inn >> antRader; inn.ignore();
	inn >> antSeter; inn.ignore();

	billetter = new int*[antRader + 1];  // Peker billetter p� et nytt variabel
	for (int i = 1; i <= antRader; i++)
		billetter[i] = new int[antSeter + 1];
	 
	for (int i = 1; i <= antRader; i++) // setter verdien til 0/tomt
		for (int j = 1; j <= antSeter; j++)
			billetter[i][j] = 0;
}

					//Displayer stoloppsettet
void Stoler::display()
{
	Sone::display();
	cout << "  ";
	int nr = 1;
	for (int i = 1; i <= antSeter; i++)
	{
		cout << nr;
		nr = ++nr % 10;
	}
	cout << "\n";
	nr = 1;
	for (int y = 1; y <= antRader; y++) // viser hvilken seter som er opptatt
	{
		for (int i = 0; i <= antSeter; i++)
		{
			if (i == 0)
			{
				cout << nr << " ";
				nr = ++nr % 10;
			}
			else if (billetter[y][i] >= 1) { cout << "x"; }
			else cout << "-";
		}
		cout << "\n";
	}
	
}
					//Skriver til steder.dta
void Stoler::skrivTilFil(ofstream & ut)
{
	ut << 'S' << '\n';
	Sone::skrivTilFil(ut);
	ut << antRader << '\n' << antSeter << '\n';
}
					//Skriver til ARR_nr.dta
void Stoler::skrivTilArrnr(ofstream & ut)
{
	
	ut << 'S' << '\n';
	Sone::skrivTilArrnr(ut);
	ut << antRader << '\n' << antSeter << '\n';
	for (int i = 1; i <= antRader; i++)
	{
		for(int j =1; j <=antSeter; j++)
		{
			ut << billetter[i][j] << ' ';
		}
		ut << '\n';
	}
	
}


void Stoler::kjopBil()
{
	bool plass = true;
	int kundeNr = 0, antBil = 0, radNr = 0, seteNr = 0;
	do
	{
		display(); // Quit
		plass = true;
		kundeNr = les("Kunde nr ditt?", 1, kundeBase.getAntKunder());
		antBil = les("Hvor mange billetter skal du ha?", 1, antSeter);
		radNr = les("Hvilken rad skal du sitte paa?", 1, antRader);
		seteNr = les("Hvilket sete skal forste mann sitte paa?", 1, antSeter);
		if ((seteNr - 1 + antBil) <= antSeter)
		{
			for (int x = seteNr; x <= (seteNr - 1 + antBil); x++)
			{
				if (billetter[radNr][x] != 0)
				{
					plass = false;
				}
			}
		}
		else { plass = false; }
		if (plass == false)
		{
			cout << "\nrad " << radNr << " fra sete " << seteNr << " har ikke plass til " << antBil << "stk\n\n";
		}
	} while (plass == false);

	ofstream ut;
	ut.open("BILLETTER.dta", std::ios::app); 
	Sone::skrivTilBil(ut);

	for (int x = seteNr; x <= (seteNr - 1 + antBil); x++)
	{
		billetter[radNr][x] = kundeNr;
		bilSolgt(); // == antSolgt++
		ut << "Rad Nr: " << radNr << " Sete Nr: " << x << "\n"; 

	}
	kundeBase.skrivTilBill(ut, kundeNr);
	ut.close();
	display();
}
void Stoler::lesFraFil( ifstream & inn) //Leser fra ARR_nr.dta
{

	for (int i = 1; i <= antRader; i++)
	{
		for (int j = 1; j <= antSeter; j++)
		{
			inn >> billetter[i][j];
			if (billetter[i][j] >= 1)
			{
				bilSolgt(); // Sone antSolgt++
			}
		}
		inn.ignore();
	}
}