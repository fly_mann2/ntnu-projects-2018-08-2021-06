#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif 
#include "Arrangement.h"
#include "funksjoner.h"
#include "enum.h"
#include <iostream>
#include "Steder.h"
#include "Stoler.h"
#include "Vrimle.h"
#include "String"
#include <sstream>

extern Steder stedBase;

// I tillegg inneholder klassen hele datastrukturen for sonene (kopi av
// aktuelt(stol)oppsett p� arrangementstedet) og status for billettsalget i disse sonene til
// ulike kunder.

Arrangement::Arrangement() {
	
	oppsett = new List(Sorted);
}
Arrangement::~Arrangement() {

	delete [] arrNavn;
	delete [] artist;
	delete oppsett;
	
}
Arrangement::Arrangement(char* _navn, int _nr) : TextElement(_navn) //Leser inn arrangementets 
{																	//data med navn og nr som parameter
	arrNr = _nr;													//sender navn til TextElement for � sorteres

	arrNavn = new char[strlen(_navn) + 1];
	strcpy(arrNavn, _navn);
	cout << "\n";

	char tmp3[STRLEN];
	cout << "Artist: ";
	cin.getline(tmp3, STRLEN);
	artist = new char[strlen(tmp3) + 1];
	strcpy(artist, tmp3);
	cout << "\n";

	dato = les("Dato: (AA/MM/DD)", MINDATO, MAXDATO); cout << "\n";
	time = les("Time: ", 0, 23);
	min = les("Min: ", 0, 59); 
	cout << "Type: ";
	char _type;
	cin >> _type; cin.ignore();  _type = toupper(_type);
	curType = getType(_type);
	

}

Arrangement::Arrangement(char* _navn, ifstream & inn) : TextElement(_navn) //Leser inn arrangementets data	
{																		   //Fra arrangementer.dta 	
	
	arrNavn = new char[strlen(_navn) + 1];
	strcpy(arrNavn, _navn); 
	

	char tmp2[STRLEN];
	inn.getline(tmp2, STRLEN);
	sted = stedBase.getSted(tmp2); 
	if (sted->compareStedNavn(tmp2) == false)
	{
		cout << "Arr maa ha et sted som finnes i stedbase!!!\n";
	}


	inn >> oppsettnr; inn.ignore();
	

	char tmp3[STRLEN];
	inn.getline(tmp3, STRLEN);
	artist = new char[strlen(tmp3) + 1];
	strcpy(artist, tmp3);
	

	inn >> arrNr; inn.ignore();
	inn >> dato; inn.ignore();
	inn >> time; inn.ignore();
	inn >> min;  inn.ignore();

	char _type;
	inn >> _type; inn.ignore();
	curType = getType(_type);


	nyttOppsett(); // Oppsett = filen Arr_nr oppsett

	
	setOppsettNr(oppsettnr);



}
void Arrangement ::display() {		//Displayer arrangementets data 

	cout << "\n";
	cout << "Arrangementets navn: " << arrNavn << "\n";
	cout << "Sted: " << sted->getNavn() << '\n';
	cout << "Artist: " << artist << "\n";
	cout << "Arrangement Nr: " << arrNr << "\n";
	cout << "Dato: (AA/MM/DD)" << dato << "\n";
	cout << "Time: " << time << ":";
	cout << min << "\n";
	cout << "Antall soner: " << oppsett->noOfElements() << "\n";
	cout << "Sjanger: ";
	switch (curType)
	{
	case type::M:
		cout << "Musikk";
		break;
	case type::S:
		cout << "Sport";
		break;
	case type::T:
		cout << "Teater";
		break;
	case type::H:
		cout << "Show";
		break;
	case type::K:
		cout << "Kino";
		break;
	case type::F:
		cout << "Familie";
		break;
	case type::E:
		cout << "Festival";
		break;
	default:
		break;
	}
	cout << "\n\n";
}

void Arrangement::endre() { 
	
} 

type Arrangement :: getType(char _type) {
	toupper(_type);
	switch (_type) {

	case 'M': return M; break;	case 'S': return S; break;
	case 'T': return T; break;	case 'H': return H; break;
	case 'K': return K; break;	case 'F': return F; break;
	case 'E': return E; break;  default : break;
	}
	return EMPTY; // empty
}
// 2) de som inneholder hele eller deler av en tekst, 
bool Arrangement::compareArrNavn(char* _arrNavn) {  
	if (strstr(arrNavn, _arrNavn) != 0 && strcmp(_arrNavn, ""))
	{
		display();
		return true;
	}
	return false;
}
// 3) spilles p� et visst sted, 
void Arrangement::compareSted(char* _stedNavn) {
	
	if (sted->compareStedNavn(_stedNavn))
	{
		display();
	}
	
} 
// 4) en viss dato,
bool Arrangement::compareDato(int _dato) {
	if (dato == _dato)
	{
		display();
		return true;
	}
	return false;
}
// 5) av en viss type(f.eks.Sport),
void Arrangement::compareType(char _type) {
	type x = getType(_type);
	if (curType == x)
	{
		display();
	}
}
// 6) en viss artist eller
void Arrangement::compareArtist(char* _artist) {
	if (0 == strcmp(artist, _artist))
	{
		display();
	}
}
// 7) alle data(inkl.billettsalget i alle soner) om et visst arrangementsnr.
bool Arrangement::compareArrNr(int _arrNr) {
	if (arrNr == _arrNr)
	{
		display();
		
		for (int i = 1; i <= oppsett->noOfElements(); i++)
		{
			Sone* x = (Sone*)oppsett->removeNo(i);
			x->display();
			cout << "\n" << "Antall Solgt: " << x->returnAntSolgt() << "\n";
			oppsett->add(x);
		}
		return true;
	}
	return false;
}
bool Arrangement::compareNavn(char* _navn) {
	if (0 == strcmp(arrNavn, _navn))
	{
		return true;
	}
	return false;
}

bool Arrangement::compDato(int d) {
	if (dato == d)
	{
		return true;
	}
	return false;
}


void Arrangement::kjopBil() {

	int valgSone; 
	if (oppsett->noOfElements() != 0) {

		ofstream ut;
		ut.open("BILLETTER.dta", std::ios::app);
		cout << "\nSkriver til filen BILLETTER.dta\n";
		ut << "\nBillett\nArrNr: " << arrNr << " ArrNavn: " << arrNavn << "\n";
		ut.close();


		valgSone = les("Velg Sone", 1, oppsett->noOfElements());
		Sone* sone = (Sone*)oppsett->removeNo(valgSone);
		sone->kjopBil();


		oppsett->add(sone);
	}
	else {
		cout << "Arr har ingen Soner\n\n";
	}
}
void Arrangement :: lesData(List* liste, char* snvn) {
	oppsett = liste;
	sted = stedBase.getSted(snvn);
}
void Arrangement :: skrivTilArrNr() { // skriver til Arr_nr filen
	
	string name = "ARR_" + to_string(arrNr) + ".dta";

	std::ofstream outfile(name);
	
	outfile << oppsett->noOfElements() << "\n";

	Sone* sone; 
	for (int i = 1; i <= oppsett->noOfElements(); i++)
	{
		sone = (Sone*)oppsett->removeNo(i); 
		sone->skrivTilArrnr(outfile);
		oppsett->add(sone);
	}

	skrivTilFil(outfile);

	cout << "Skriver til " << name << "\n";

	outfile.close();
}
int Arrangement :: getArrNr() {
	return arrNr;
}
void Arrangement::skrivTilBil(ofstream & ut) {
}

void Arrangement::skrivTilFil(ofstream & ut)
{
	ut << arrNavn << '\n' << sted->getNavn() << '\n' << oppsettnr << '\n' << artist << '\n' << arrNr << '\n'
		<< dato << '\n' << time << '\n' << min << '\n';
	switch (curType)
	{
	case type::M: ut << 'M'; break;	case type::S: ut << 'S'; break;
	case type::T: ut << 'T'; break;	case type::H: ut << 'H'; break;
	case type::K: ut << 'K'; break;	case type::F: ut << 'F'; break;
	case type::E: ut << 'E'; break; default:      ut << "X"; break;
	}
	ut << '\n';

}
void  Arrangement::setOppsettNr(int oppNR) {
	oppsettnr = oppNR;
}

void Arrangement::nyttOppsett() {

	string name = "ARR_" + to_string(arrNr) + ".dta";
	ifstream inn2(name);

	oppsett = new List(Sorted);
	int soner;
	inn2 >> soner; inn2.ignore();
	for (int j = 1; j <= soner; j++)
	{
		char type1;
		inn2 >> type1; inn2.ignore();
		char navn[NAVNLEN];
		inn2.getline(navn, NAVNLEN);

		if (type1 == 'S')							//Type = stol 
		{
			Stoler* sone = new Stoler(navn, inn2); // nytt sone
			oppsett->add(sone);
			sone->lesFraFil(inn2);					// leser fra Arr_nr aa forandrer p� Sonen
			// Arr_nr er veldig lik Steder sin fil(Sone info), men innholder billetter etter sone info

		}
		else if (type1 == 'V')					   //Type = vrimle
		{
			Vrimle* sone = new Vrimle(navn, inn2);
			oppsett->add(sone);
			sone->lesFraFil(inn2);

		}
	}
	inn2.close();
}



