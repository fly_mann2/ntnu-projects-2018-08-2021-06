#if !defined(__Kunder_H)
#define  __Kunder_H

#include "ListTool2B.H"
#include "Kunde.h"

class Kunder  
{
private:
	int antKunder;
	List* kunder;

public :

	Kunder();
	~Kunder();
	void valg();
	void display();
	void display(int n);
	
	void lesFraFil();
	void nyKunde();
	void endreKunde();
	void skrivTilFil();
	int getAntKunder();
	void skrivTilBill(ofstream & ut, int nr);

};




#endif