#include <iostream>
#include <fstream>
#include "funksjoner.h"
#include "Kunder.h"

using namespace std;

			//leser ett tegn
char les() {
	char ch;
	cout << "\n\nVelg kommando: ";
	cin >> ch; cin.ignore();

	return (toupper(ch));
}

			// leser en int i et intervall
int les(const char* t, int min, int max) {
	int tall;
	do {
		cout << t << " (" << min << '-' << max << "):  ";
		cin >> tall;  cin.ignore();
	} while (tall < min || tall > max);
	return tall;
}

void skrivMeny() {

	cout << "\n\nDisse valgene er tilgjengelige: ";
	cout << "\nFOR KUNDER: ";
	cout << "\n\tK D - display kunde(r)";
	cout << "\n\tK N - legg til ny kunde";
	cout << "\n\tK E - endre kunde-data";
	cout << "\n\nFOR STEDER: ";
	cout << "\n\tS D - display sted(er)";
	cout << "\n\tS N - legg til nytt spille-/arrangementsted";
	cout << "\n\nFOR OPPSETT: ";
	cout << "\n\tO D - display oppsett";
	cout << "\n\tO N - legg til nytt (stol)oppsett";
	cout << "\n\tO E - endre oppsett";
	cout << "\n\nFOR ARRANGEMENTER: ";
	cout << "\n\tA D - display arrangement(er)";
	cout << "\n\tA N - legg til arrangement";
	cout << "\n\tA S - slett et arrangement";
	cout << "\n\tA K - kjop billett(er) til arrangement";
	cout << "\n\n\tQ - avslutt";
}

