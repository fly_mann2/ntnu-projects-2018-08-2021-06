#if !defined(__Sted_H)
#define  __Sted_H

#include "CONST.h"
#include <fstream>
#include "ListTool2B.H"

using namespace std;

class Sted : public TextElement
{
private:
	char* stedsnavn;
	int antOppsett;
	List* oppsett[MAXOPPSETT + 1]; // starter p� 1
	
public:
	
	~Sted();
	Sted(char* stedsnavn);
	Sted(char* navn, ifstream & inn);
	void skrivTilFil(ofstream & ut);
	List* kopier(int _nr);
	bool compareStedNavn(char* _stedNavn);
	void displayOppsett(int nr);
	
	void display();
	void nyttOppsett();

	void endreOppsett(int nr);
	int getAnt();
	char* getNavn();
	


};













#endif


