#if !defined(__Vrimle_H)
#define  __Vrimle_H

#include "Sone.h"

class Vrimle : public Sone
{

private:
	int* billetter;
	int kundenr;
	int sisteBillettKjopt;


public:
	~Vrimle();
	Vrimle(char* navn);
	
	Vrimle(char* navn,ifstream & inn);
	void skrivTilFil(ofstream & ut);
	void display();
	void kjopBil();

	void lesFraFil(ifstream & inn);


	void skrivTilArrnr(ofstream & ut);


};


#endif