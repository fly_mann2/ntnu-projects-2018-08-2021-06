#if !defined(__Arrangement_H)
#define  __Arrangement_H
#include <fstream>
#include "Const.h"
#include "Enum.h"
#include "Sted.h"
#include "Sone.h"
#include "ListTool2B.H"
#include "Arrangementer.h"


using namespace std;

class Arrangement : public TextElement {

private:
	char* arrNavn; // inneholder dets navn(sortert p� dette, men ikke unikt),
	Sted* sted;
	char* artist;
	int arrNr;     // et unikt arrangementsnummer
	int dato;
	int time;
	int min;
	List* oppsett;


	type curType;

	int oppsettnr;

public:
	Arrangement();
	~Arrangement();
	Arrangement(char* _navn, int _nr);
	Arrangement(char* _navn, ifstream & inn);
	void skrivTilFil(ofstream & ut);
	void display();

	void endre();


	type getType(char _type);
	bool compareArrNavn(char* _arrNavn);
	void compareSted(char* _arrNavn);
	bool compareDato(int _arrNavn);
	void compareType(char _arrNavn);
	void compareArtist(char* _arrNavn);
	
	bool compareArrNr(int _arrNr);
	bool compareNavn(char* _navn);

	bool compDato(int d);

	

	
	void kjopBil();
	void lesData(List* liste, char* snvn);
	void skrivTilArrNr();
	int getArrNr();
	void skrivTilBil(ofstream & ut);
	void setOppsettNr(int oppNR);
	void nyttOppsett();

		

};


#endif
