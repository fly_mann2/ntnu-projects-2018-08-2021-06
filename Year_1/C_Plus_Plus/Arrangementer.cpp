#include "Arrangement.h"
#include "Arrangementer.h"
#include "funksjoner.h"
#include <iostream>
#include "CONST.h"
#include "Steder.h"
#include "String"

extern Steder stedBase;
extern Kunder kundeBase;


Arrangementer::Arrangementer()
{

	arrangementer = new List(Sorted);
	sisteArrNr = arrangementer->noOfElements();
}

Arrangementer::~Arrangementer()
{
	delete arrangementer;
}
						//Leser fra Arrangementer.dta
void Arrangementer::lesFraFil()
{
	ifstream inn("Arrangementer.dta");
	Arrangement* nyArr;
	if (inn)
	{
		inn >> sisteArrNr; inn.ignore();
		cout << "\nleser fra filen Arrangementer.dta\n";

		for (int i = 1; i <= sisteArrNr; i++)
		{
			char _navn[STRLEN];
			inn.getline(_navn, STRLEN);
			nyArr = new Arrangement(_navn, inn);
			arrangementer->add(nyArr);
		}
	}
	else
		cout << "Finner ikke filen Arrangementer.dta";
}

void Arrangementer::valg() {
	char valg;
	cin >> valg; valg = toupper(valg); cin.ignore();

	switch (valg)
	{
	case 'D':						//Display

		int valg2;
		valg2 = les(
			"1) alle arrangement,\n"
			"2) de som inneholder hele eller deler av en tekst,\n"
			"3) spilles p� et visst sted,\n"
			"4) en viss dato, \n"
			"5) av en viss type(f.eks.Sport), \n"
			"6) en viss artist eller \n"
			"7) alle data(inkl.billettsalget i alle soner) om et visst arrangementsnr.\n",
			1, 7); cout << "\n";
		switch (valg2)
		{
		case 1:
			arrangementer->displayList();
			break;
		case 2:
			char _name[STRLEN];
			cin.getline(_name, STRLEN); cout << "\n";
			for (int i = 1; i <= sisteArrNr; i++)
			{
				Arrangement* x = (Arrangement*)arrangementer->removeNo(i);
				x->compareArrNavn(_name);
				arrangementer->add(x);
			}
			break;
		case 3:
			char _stedNavn[STRLEN];
			cin.getline(_stedNavn, STRLEN); cout << "\n";
			for (int i = 1; i <= sisteArrNr; i++)
			{
				Arrangement* x = (Arrangement*)arrangementer->removeNo(i);
				x->compareSted(_stedNavn);
				arrangementer->add(x);
			}
			break;
		case 4:
			int _dato;
			_dato = les("Dato: (AA/MM/DD)", MINDATO, MAXDATO); cout << "\n";
			for (int i = 1; i <= sisteArrNr; i++)
			{
				Arrangement* x = (Arrangement*)arrangementer->removeNo(i);
				x->compareDato(_dato);
				arrangementer->add(x);
			}
			break;
		case 5:

			char _type;
			_type = les();

			for (int i = 1; i <= sisteArrNr; i++)
			{
				Arrangement* x = (Arrangement*)arrangementer->removeNo(i);
				x->compareType(_type);
				arrangementer->add(x);
			}

			break;
		case 6:
			char _artist[STRLEN];
			cin.getline(_artist, STRLEN); cout << "\n";
			for (int i = 1; i <= sisteArrNr; i++)
			{
				Arrangement* x = (Arrangement*)arrangementer->removeNo(i);
				x->compareArtist(_artist);
				arrangementer->add(x);
			}
			break;
		case 7: //  7) alle data (inkl. billettsalget i alle soner) om et visst arrangementsnr.
			int _arrNr;
			_arrNr = les("Arrangementsnr: ", 1, sisteArrNr); cout << "\n";
			for (int i = 1; i <= sisteArrNr; i++)
			{
				Arrangement* x = (Arrangement*)arrangementer->removeNo(i);
				x->compareArrNr(_arrNr);
				arrangementer->add(x);
			}
			break;
		default:
			break;
		}
		break;
	case 'N':			//Ny
		ny();
		break;
	case 'E':			
		break;
	case 'S':			//Slett
		

		char navn[STRLEN]; 
		if (sisteArrNr >= 1)
		{
			do
			{
				cout << "\nArr Navn du vil slette fra\neller 0 for aa display alle: ";
				cin.getline(navn, STRLEN);

				if (strcmp(navn, "0") == 0)
				{
					arrangementer->displayList();
				}
			} while (strcmp(navn, "0") == 0);			//S� lenge bruker skriver '0'

			int a; // Sjekker hvor mange Arr det finnes med "navnk"
			a = 0;
			for (int i = 1; i <= sisteArrNr; i++)
			{
				Arrangement* arr = (Arrangement*)arrangementer->removeNo(i);
				if (arr->compareArrNavn(navn))
				{
					a++;
				}
				arrangementer->add(arr);
			}
			if (a >= 1)
			{
				int dato;
				dato = 0;
				if (a >= 2) // Finnes flere med samme navn
				{
					cout << "\n" << a << " Arr med navnet: " << navn << "\n";
					dato = les("Dato til Arr? ", MINDATO, MAXDATO);  // Hvilken dato har arrangementet
				}
				bool fant = false; // funnet en aa slette?
				for (int i = 1; i <= sisteArrNr; i++)
				{
					fant = false;
					Arrangement* arr = (Arrangement*)arrangementer->removeNo(i);
					if (a == 1)
					{
						if (arr->compareArrNavn(navn))
						{
							fant = true;
						}
					}
					else {
						if (arr->compareDato(dato))
						{
							fant = true;
						}
						else if (i == sisteArrNr)
						{
							cout << "ingen med dato: " << dato << "\n";
						}
					}
					if (fant == false) // legger kun tilbake i listen hvis Arr ikke skal slettes
					{
						arrangementer->add(arr);
					}
					else if (fant == true) {
						string name = "ARR_" + to_string(arr->getArrNr()) + ".dta";
						delete arr;							// sletter ARR
						sisteArrNr--;
						cout << "\nArrangement slettet\n"; 
						if (remove(name.c_str()) != 0)	 // sletter filen ARR_nr
							perror("Kunne ikke slette filen");
						else
							puts("Fil slettet");
					}
				}
			}
			else
			{
				cout << "ingen med navnet: " << navn << "\n";
			}
		}
		else
		{
			cout << "Finnes ingen Arr\n\n";
		}

		break;

	case 'K':				//Kjop
		char navnk[STRLEN]; // arr navn

		if (sisteArrNr >= 1)
		{
			do
			{
				cout << "\nArr Navn du vil kjope fra\neller 0 for aa display alle: ";
				cin.getline(navnk, STRLEN);

				if (strcmp(navnk, "0") == 0)
				{
					arrangementer->displayList();
				}
			} while (strcmp(navnk, "0") == 0);			//S� lenge du skriver '0'

			int a2; // Sjekker hvor mange Arr det finnes med "navnk"
			a2 = 0;
			for (int i = 1; i <= sisteArrNr; i++)
			{
				Arrangement* arr = (Arrangement*)arrangementer->removeNo(i);
				if (arr->compareArrNavn(navnk))
				{
					a2++;
				}
				arrangementer->add(arr);
			}
			if (a2 >= 1)
			{
				int datoA;
				datoA = 0;
				if (a2 >= 2) // Finnes flere med samme navn
				{
					cout << "\n" << a2 << " Arr med navnet: " << navnk << "\n";
					datoA = les("Dato til Arr? ", MINDATO, MAXDATO); // Hvilken dato har sonen
				}
				for (int i = 1; i <= sisteArrNr; i++)
				{
					Arrangement* arr = (Arrangement*)arrangementer->removeNo(i);
					if (a2 == 1)
					{
						if (arr->compareArrNavn(navnk)) 
						{
							arr->kjopBil();
							arr->skrivTilArrNr();
						}
					}
					else { // Hvis flere arr med samme navn
						if (arr->compareDato(datoA)) 
						{
							arr->kjopBil();
							arr->skrivTilArrNr();
						}
						else if (i == sisteArrNr)
						{
							cout << "ingen med dato: " << datoA << "\n";
						}
					}
					arrangementer->add(arr);
				}
			}
			else
			{
				cout << "ingen med navnet: " << navnk << "\n";
			}
		}
		else
		{
			cout << "Finnes ingen Arr\n\n";
		}
		
		
		break;
	case 'R':
		break;
	}
}
void Arrangementer::display(const char* _navn)
{
	arrangementer->displayElement(_navn);
}
void Arrangementer::ny() {
	char anvn[STRLEN], snvn[STRLEN];
	int  nr, ant;
	Arrangement* arrangement;
	List* liste;

	// les("Sted", snvn, STRLEN);
	cout << "Sted ";
	cin.getline(snvn, STRLEN);

	Sted* stedX;
	if (stedBase.getSted(snvn) != nullptr)
	{
		stedX = stedBase.getSted(snvn);
		ant = stedX->getAnt();

		if (ant >= 1) {
			cout << "\nNytt arrangements navn: ";
			cin.getline(anvn, STRLEN);
			nr = les("Oppsettnummer", 1, ant);
			arrangement = new Arrangement(anvn, ++sisteArrNr);
			liste = stedBase.kopier(snvn, nr);

			arrangement->setOppsettNr(nr); // Kopier

			arrangement->lesData(liste, snvn); // Oppsett = kopi

			arrangement->skrivTilArrNr(); // Skriver til arr<nr> filen


			arrangement->nyttOppsett(); // Leser inn fra arr<nr> filen


			arrangementer->add(arrangement);
			cout << "\n";

		}
		else
			cout << "\n\n\tIngen (stol)oppsett registrert p� dette stedet!\n\n";
	}
	else { cout << "\n\n\tIngen arrangementsted med dette navnet!\n\n"; }



}


						//Skriver til Arrangementer.dta
void Arrangementer::skrivTilFil()
{
	ofstream ut("Arrangementer.dta");

	ut << sisteArrNr << '\n';

	for (int i = 1; i <= sisteArrNr; i++)
	{
		Arrangement* temp = (Arrangement*)arrangementer->removeNo(i);
		temp->skrivTilFil(ut);
		arrangementer->add(temp);
	}
	cout << "\nSkriver til filen Arrangementer.dta\n";
}

