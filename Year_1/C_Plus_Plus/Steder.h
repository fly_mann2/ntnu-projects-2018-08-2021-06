#if !defined(__Steder_H)
#define  __Steder_H

#include "ListTool2B.H"
#include "Sted.h"

class Steder 
{

private:
	int antSteder;
	List * steder;

public:

	Steder();
	~Steder();
	void lesFraFil();
	void valg(char valg);
	void display();
	void nyttSted();
	void skrivTilfil();
	List* kopier(char* nvn, int nr);

	Sted* getSted(char* navn);


};




#endif