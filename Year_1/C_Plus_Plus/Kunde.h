#if !defined(__Kunde_H)
#define  __Kunde_H

#include <fstream>
#include "ListTool2B.H"
using namespace std;


class Kunde : public NumElement
{

private:
	int kundenr;
	int postnr;
	int tlf;
	char* navn;
	char* adresse;
	char* poststed;
	char* epost;


public:
	Kunde();
	Kunde(int kundenr);
	Kunde(int n, ifstream & inn);
	~Kunde();
	void display();
	void compNavn(char* n);
	void skrivTilFil(ofstream & ut);
	void skrivTilBill(ofstream & ut);
	



};

#endif


