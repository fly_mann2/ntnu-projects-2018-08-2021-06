#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif 

#include <iostream>
#include "Sted.h"
#include "funksjoner.h"
#include "Steder.h"
#include "Sone.h"
#include "Stoler.h"
#include "Vrimle.h"
#include "CONST.h"

using namespace std;

Sted::~Sted()
{
	delete[] stedsnavn;
						//Sletter alle oppsett
	for (int i = 1; i <= antOppsett; i++)
	{
		delete oppsett[i];
	}

}

Sted::Sted(char* navn) : TextElement(navn) { //Setter stedsnavn til det som
											 //blir sendt med som parameter
	stedsnavn = new char[strlen(navn) + 1];  //og sender navnet opp til
	strcpy(stedsnavn, navn);				 //TextElement for sortering

}

Sted::Sted(char* n, ifstream & inn) : TextElement(n) { //Leser sted og
													   //oppsett fra fil
	stedsnavn = new char[strlen(n) + 1];
	strcpy(stedsnavn, n);

	inn >> antOppsett; inn.ignore();

	for (int i = 1; i <= antOppsett; i++)
	{
		oppsett[i] = new List(Sorted);
		int soner;
		inn >> soner; inn.ignore();
		for (int j = 1; j <= soner; j++)
		{
			char type;
			inn >> type; inn.ignore();
			char navn[NAVNLEN];
			inn.getline(navn, NAVNLEN);
			if (type == 'S')							//Type = stol 
			{
				oppsett[i]->add(new Stoler(navn, inn));
			}
			else if (type == 'V')					   //Type = vrimle
			{
				oppsett[i]->add(new Vrimle(navn, inn));
			}
		}

	}
}


void Sted::display() { //Displayer stedets data

	cout << "\nNavn: " << stedsnavn << "\nAntall oppsett: "
		<< antOppsett << endl;
}
// returnerer en List med soner "Oppsett"
List* Sted::kopier(int nr) { // nr = oppsett aa kopiere
	List* liste = NULL;
	int i, ant;
	Sone *sone, *kopi;

	if (nr >= 1 && nr <= MAXOPPSETT) {
		ant = oppsett[nr]->noOfElements();

		liste = new List(Sorted);
		for (i = 1; i <= ant; i++) { 
			sone = (Sone*)oppsett[nr]->removeNo(i); // fjerner sone midlertidig fra sted oppsett
			if (sone->getType() == 'S')  kopi = new Stoler(*((Stoler*)sone)); // ser om det er stol eller vrimle
			else kopi = new Vrimle(*((Vrimle*)sone));
			oppsett[nr]->add((Element*)sone); // returnerer sonen
			liste->add((Element*)kopi); // adder kopien til listen 
		}
	}
	return liste;
}

bool Sted::compareStedNavn(char* _stedNavn) { //Sammenlikner stedsnavnet
	if (strcmp(stedsnavn, _stedNavn) == 0)
	{
		return true;
	}
	return false;
}
void Sted::displayOppsett(int nr)			//Displayer alle soner p� ett
{											//oppsett 
	if (antOppsett != 0)
	{
		oppsett[nr]->displayList();

	}
	else
	{
		cout << "\nIngen oppsett � displaye\n";
	}


}


void Sted::nyttOppsett()	//Lager ett nytt oppsett p� et sted
{

	oppsett[++antOppsett] = new List(Sorted);

	int valg = les("1 = Lage helt nytt oppsett\n"
		"2 = Kopiere og endre p� et alleredeeksisterende oppsett\n", 1, 2);

	if (valg == 2 && antOppsett <= 1) //Hvis det ikke finnes oppsett � kopiere
	{
		cout << "Ingen oppsett aa kopiere fra\nLages nytt oppsett\n";
		valg = 1;
	}
	if (valg == 1) //Lager helt nytt oppsett
	{
		char snavn[NAVNLEN];

		cout << "\nSonenavn (0 for a avslutte): ";
		cin.getline(snavn, NAVNLEN);


		while (strcmp(snavn, "0")) //G�r frem til sonenavnet er 0
		{
			char type;

			do //Stol eller vrimle oppsett
			{
				cout << "\nS(tol)/V(rimle)?: "; type = les();

			} while (type != 'S' && type != 'V');

			if (type == 'S')
			{
				oppsett[antOppsett]->add(new Stoler(snavn));
			}
			else
			{
				oppsett[antOppsett]->add(new Vrimle(snavn));
			}

			cout << "\nSonenavn (0 for a avslutte): ";
			cin.getline(snavn, NAVNLEN);
		}

	}
	else if (valg == 2) //Kopierer ett oppsett som kan endres p�
	{
		int valg2 = les("Hvilket oppsett vil du kopiere?", 1, antOppsett - 1);
		oppsett[antOppsett] = kopier(valg2);
		displayOppsett(antOppsett);
		endreOppsett(antOppsett);
	}



}



void Sted::endreOppsett(int nr)
{
	int valg;

	valg = les("\n\nHva vil du gjore?\n"
		"1-Endre sone\n"
		"2-Legg til sone\n"
		"3-Fjerne sone\n"
		"0-Avslutt", 0, 3);


	while (valg != 0)
	{
		switch (valg)
		{
		case 1:
			char __snavn[NAVNLEN];
			cout << "\nHvilken sone vil du endre pa?: ";
			cin.getline(__snavn, NAVNLEN);
			if (oppsett[nr]->inList(__snavn)) //Hvis sonen finnes slettes den
			{								  //og det blir laget en ny
				oppsett[nr]->destroy(__snavn); //med samme navn
				cout << "\nNy sone:\n\n";
				char type;
				do
				{
					cout << "\nS(tol)/V(rimle)?: "; type = les();

				} while (type != 'S' && type != 'V');

				if (type == 'S')
				{
					oppsett[nr]->add(new Stoler(__snavn));
				}
				else
				{
					oppsett[nr]->add(new Vrimle(__snavn));
				}
				break;

			}
			else
			{
				cout << "\nIngen sone med dette navnet\n";

			}
			break;
		case 2: //Lager en ny sone
			char snavn[NAVNLEN], type;
			cout << "\nSonenavn: "; cin.getline(snavn, NAVNLEN);

			do
			{
				cout << "\nS(tol)/V(rimle)?: "; type = les();

			} while (type != 'S' && type != 'V');

			if (type == 'S')
			{
				oppsett[nr]->add(new Stoler(snavn));
			}
			else
			{
				oppsett[nr]->add(new Vrimle(snavn));
			}
			break;
		case 3:
			char _snavn[NAVNLEN];
			cout << "\nHvilken sone vil du slette?: ";
			cin.getline(_snavn, NAVNLEN);
			if (oppsett[nr]->inList(_snavn)) //Sletter sone hvis den finnes
			{
				oppsett[nr]->destroy(_snavn);
				cout << "\nSone slettet...\n";
			}
			else
			{
				cout << "\nIngen sone med dette navnet\n";

			}
			break;
		default: break;

		}
		valg = les("\nHva vil du gjore?\n"
			"1-Endre sone\n"
			"2-Legg til sone\n"
			"3-Fjerne sone\n"
			"0-Avslutt", 0, 3);

	}
}

int Sted::getAnt()
{
	return antOppsett;
}

char* Sted::getNavn()
{
	return stedsnavn;
}

void Sted::skrivTilFil(ofstream & ut) { //Skriver sone og oppsett til fil

	Sone* temp = nullptr;
	ut << '\n' << stedsnavn << '\n' << antOppsett << endl;
	for (int i = 1; i <= antOppsett; i++) {

		int soner = oppsett[i]->noOfElements();
		ut << soner << '\n';
		for (int j = 1; j <= soner; j++)
		{
			temp = (Sone*)oppsett[i]->removeNo(j);
			temp->skrivTilFil(ut);
			oppsett[i]->add(temp);
		}
	}

}

