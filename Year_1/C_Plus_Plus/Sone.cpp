#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif 

#include <iostream>
#include "Sone.h"
#include "CONST.h"


using namespace std;


Sone::Sone()
{
}

Sone::~Sone()
{
	delete[] sonenavn;
}
						//Leser inn sonens data
Sone::Sone(char* navn) : TextElement(navn)
{
	sonenavn = new char[strlen(navn) + 1];
	strcpy(sonenavn, navn);

	
	cout << "\nPris pr billett: "; cin >> pris;

}
						//Displayer sonens data
void Sone::display()
{
	cout << "\n\nSonenavn: " << sonenavn;
	cout << "    Antall billetter: " << antBilletter;
	cout << "    Pris: " << pris << "\n\n";
}


						//Leser sonens data fra fil
Sone::Sone(char* navn, ifstream & inn) : TextElement(navn)
{

	sonenavn = new char[strlen(navn) + 1];
	strcpy(sonenavn, navn);

	inn >> antBilletter; inn.ignore();
	inn >> pris; inn.ignore();
	

}
						//Skriver til Steder.dta
void Sone::skrivTilFil(ofstream & ut)
{
	ut << sonenavn << '\n';
	ut << antBilletter << '\n';
	ut << pris << '\n';
	

}
					  //Skriver til ARR_nr.dta
void Sone::skrivTilArrnr(ofstream & ut)
{
	
	ut << sonenavn << '\n';
	ut << antBilletter << '\n' << pris << '\n'; 
}

char Sone::getType() {	//Sone eller vrimle
	return avType;
}
						
void Sone::setAntb(int rader, int seter) {
	antBilletter = rader * seter;
}

void Sone::kjopBil() {

}

void Sone:: bilSolgt() {	//Plusser p� antall solgt
	antSolgt++;
}
						//Returnerer antall billetter
int Sone :: getAntallBilletter() {

	return antBilletter;

}
					   //Returnerer antall billetter som er solgt
int Sone::returnAntSolgt() {

	return antSolgt;
}
						//Setter avType til Vrimle
void Sone::setType(char t)
{

	avType = t;
}
					//Skriver til billetter.dta
void Sone::skrivTilBil(ofstream & ut) {
	ut << "Pris : " << pris << " SoneNavn: " << sonenavn << "\n";
}
				   //Leser fra ARR_nr.dta
void Sone:: lesFraFil(ifstream & inn)  {
	
	inn.getline(sonenavn , STRLEN);
	inn >> antBilletter; inn.ignore();
	inn >> pris; inn.ignore();
	inn >> antSolgt; inn.ignore();
}
